﻿using PainelEletronicoWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PainelEletronicoWeb.Controllers
{
    public class RegistroPresencaController : Controller
    {
        // GET: RegistroPresenca
        public ActionResult Index()
        {
            var presenca = new PresencaModel().Presenca();
            return View(presenca);
        }
    }
}