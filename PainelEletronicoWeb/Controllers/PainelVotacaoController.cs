﻿using PagedList;
using PainelEletronicoWeb.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace PainelEletronicoWeb.Controllers
{
    public class PainelVotacaoController : Controller
    {
        [Route("painel/{id?}")]
        public ActionResult CamaraMunicipal(string id)
        {
            try
            {
                var model = new CamaraMunicipalModel(id);
                return PartialView("CamaraMunicipal", model);
            }
            catch (Exception ex)
            {
                ViewBag.MensagemErro = ex.Message;
                return PartialView("../Erro");
            }
        }

        public string ObterRelogio(Guid idCamara, string tipo)
        {
            try
            {
                string template = "{0}:{1}:{2}";
                RelogioModel.Obter(idCamara, tipo, ref template);
                return template;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [Authorize(Roles = "Admin")]
        public string IniciarPausarRelogio(Guid idCamara, string tipo, int intervalo,string botao)
        {
            try
            {
                return RelogioModel.IniciarPausar(idCamara, tipo, intervalo,botao);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [Authorize(Roles = "Admin")]
        public string PararRelogio(Guid idCamara, string tipo)
        {
            try
            {
                return RelogioModel.Parar(idCamara, tipo);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public ActionResult ListaVereadores(string id)
        {
            try
            {
                var model = new CamaraMunicipalModel(id);
                var vereadores = model.Vereadores;

                int itensPorPagina = 13;
                int pagina = 1;
                decimal totalPaginas = Math.Ceiling(Convert.ToDecimal(vereadores.Count()) / Convert.ToDecimal(itensPorPagina));

                if (HttpContext.Session["Pagina"]==null)
                {
                    HttpContext.Session.Add("Pagina", pagina);
                }
                else
                {
                    pagina = Convert.ToInt32(HttpContext.Session["Pagina"])+1;
                    if (pagina > totalPaginas )
                        pagina = 1;
                    HttpContext.Session.Remove("Pagina");
                    HttpContext.Session.Add("Pagina", pagina);
                }
                return PartialView("ListaVereadores", vereadores.ToPagedList(pagina,itensPorPagina));
            }
            catch (Exception ex)
            {
                ViewBag.MensagemErro = ex.Message;
                return PartialView("../Erro");
            }
        }
    }
}