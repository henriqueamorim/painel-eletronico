﻿using System;
using System.Web.Mvc;
using PainelEletronicoWeb.Models;

namespace PainelEletronicoWeb.Controllers
{
    public class VereadorController : Controller
    {
        [Authorize(Roles = "Superadmin")]
        public JsonResult Salvar(VereadorModel model)
        {
            try
            {
                bool suc = model.Salvar();
                return Json(new { sucesso = suc, id = model.ID });
            }
            catch (Exception e)
            {
                return Json(new { sucesso = false, erro = e.Message });
            }
        }

        [Authorize(Roles = "Superadmin")]
        public JsonResult Excluir(VereadorModel model)
        {
            try
            {
                bool suc = model.Excluir();
                return Json(new { sucesso = suc, id = model.ID });
            }
            catch (Exception e)
            {
                return Json(new { sucesso = false, erro = e.Message });
            }
        }
    }
}