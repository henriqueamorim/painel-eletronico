﻿using System;
using PainelEletronicoWeb.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web;

namespace PainelEletronicoWeb.Controllers
{
    public class CadastrosController : Controller
    {
        [Authorize(Roles = "Superadmin")]
        public ActionResult CamaraMunicipal()
        {
            try
            {
                CamaraMunicipalModel m = new CamaraMunicipalModel();
                var model = m.Listar();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.MensagemErro = ex.Message;
                return PartialView("../Erro");
            }
        }

        [Authorize(Roles = "Superadmin")]
        public ActionResult Vereador(Guid? idCamara)
        {
            try
            {
                HttpContext.Session.Remove("IDCamaraVereador");
                HttpContext.Session.Add("IDCamaraVereador", idCamara);
                VereadorModel m = new VereadorModel();
                var model = m.Listar(idCamara == null ? Guid.Empty : (Guid)idCamara);
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.MensagemErro = ex.Message;
                return PartialView("../Erro");
            }
        }

        [Authorize(Roles = "Superadmin,Admin")]
        public ActionResult Votacao()
        {
            try
            {
                VotacaoModel m = new VotacaoModel();
                IEnumerable<VotacaoModel> model = new List<VotacaoModel>();
                if(User.IsInRole("Superadmin"))
                    model = m.Listar();
                else
                    model = m.Listar(CamaraMunicipalModel.ObterID(User.Identity.Name));
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.MensagemErro = ex.Message;
                return PartialView("../Erro");
            }
        }
    }
}
