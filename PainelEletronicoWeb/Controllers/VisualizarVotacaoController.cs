﻿using PainelEletronicoWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PainelEletronicoWeb.Controllers
{
    public class VisualizarVotacaoController : Controller
    {
        // GET: VisualizarVotacao
        public ActionResult Index()
        {
            var votacoes = new VotoModel().TrazerVotos();
            return View(votacoes);
        }
    }
}