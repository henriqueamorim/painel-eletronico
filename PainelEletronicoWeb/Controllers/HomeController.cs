﻿using System;
using System.Web.Mvc;
using PainelEletronicoWeb.Models;

namespace PainelEletronicoWeb.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            try
            {
                if(User.Identity.IsAuthenticated && User.IsInRole("Admin"))
                {
                    string diretorio = CamaraMunicipalModel.ObterDiretorio(User.Identity.Name);
                    var model = new CamaraMunicipalModel(diretorio);
                    ViewBag.IDCamaraMunicipal = model.ID;
                    ViewBag.TempoUsoAparte = model.TempoUsoAparte;
                    ViewBag.TempoUsoPalavra = model.TempoUsoPalavra;
                    ViewBag.Diretorio = diretorio;
                }
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.MensagemErro = ex.Message;
                return PartialView("../Erro");
            }

        }
    }
}
