﻿using System;
using System.Web.Mvc;
using PainelEletronicoWeb.Models;
using System.Web;
using Microsoft.AspNet.Identity.Owin;

namespace PainelEletronicoWeb.Controllers
{
    public class CamaraMunicipalController : Controller
    {
        [Authorize(Roles = "Superadmin")]
        [HttpPost]
        public JsonResult Salvar(CamaraMunicipalModel model, HttpPostedFileBase file)
        {
            try
            {
                ApplicationUserManager userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                bool suc = model.Salvar(userManager);
                return Json(new { sucesso = suc, id = model.ID });
            }
            catch (Exception e)
            {
                return Json(new { sucesso = false, erro = e.Message });
            }
        }

        [Authorize(Roles = "Superadmin")]
        public JsonResult Excluir(CamaraMunicipalModel model)
        {
            try
            {
                bool suc = model.Excluir();
                return Json(new { sucesso = suc, id = model.ID });
            }
            catch (Exception e)
            {
                return Json(new { sucesso = false, erro = e.Message });
            }
        }
    }
}