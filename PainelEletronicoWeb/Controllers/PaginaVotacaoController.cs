﻿using System;
using System.Web.Mvc;
using PainelEletronicoWeb.Models;

namespace PainelEletronicoWeb.Controllers
{
    public class PaginaVotacaoController : Controller
    {
        [HttpPost]
        public JsonResult Autenticar(Guid idVereador, string senha)
        {
            try
            {
                VotoModel vt = new VotoModel(idVereador, senha);
                vt.RegistrarPresenca();
                return Json(new { sucesso = true });
            }
            catch (Exception e)
            {
                return Json(new { sucesso = false, erro = e.Message });
            }
        }

        [HttpPost]
        public JsonResult Votar(Guid idVereador, string senha, string voto)
        {
            try
            {
                VotoModel vt = new VotoModel(idVereador, senha, voto);
                return Json(new { sucesso = vt.Salvar() });
            }
            catch (Exception e)
            {
                return Json(new { sucesso = false, erro = e.Message });
            }
        }

        [HttpPost]
        public JsonResult ObterTotais(Guid idSessao)
        {
            try
            {
                int totalGeral = 0;
                int totalPresentes = 0;
                int totalAusentes = 0;
                int totalSim = 0;
                int totalNao = 0;
                int totalAbstencao = 0;
                string resultadoVotacao = string.Empty;

                VotacaoModel.ObterTotais(idSessao, ref totalGeral, ref totalPresentes, ref totalAusentes, ref totalSim, ref totalNao, ref totalAbstencao, ref resultadoVotacao);

                return Json(new {
                    TotalGeral = totalGeral,
                    TotalPresentes = totalPresentes,
                    TotalAusentes = totalAusentes,
                    TotalSim = totalSim,
                    TotalNao = totalNao,
                    TotalAbstencao = totalAbstencao,
                    ResultadoVotacao = resultadoVotacao
                });
            }
            catch (Exception e)
            {
                return Json(new { sucesso = false, erro = e.Message });
            }
        }

        [Route("votacao/{id?}")]
        public ActionResult CamaraMunicipal(string id)
        {
            try
            {
                var model = new CamaraMunicipalModel(id);
                var vModel = new VotoModel();

                VereadorModel ver = vModel.ObterCredenciais(model.ID);

                if(ver != null)
                {
                    ViewBag.Autenticado = true;
                    ViewBag.IDVereador = ver.ID;
                    ViewBag.NomeVereador = ver.Nome;
                }
                else
                {
                    ViewBag.Autenticado = false;
                }

                return PartialView("CamaraMunicipal", model);
            }
            catch (Exception ex)
            {
                ViewBag.MensagemErro = ex.Message;
                return PartialView("../Erro");
            }
        }

        [Authorize(Roles = "Superadmin,Admin")]
        public JsonResult Salvar(VotacaoModel model)
        {
            try
            {
                bool suc = model.Salvar();
                return Json(new { sucesso = suc, id = model.ID });
            }
            catch (Exception e)
            {
                return Json(new { sucesso = false, erro = e.Message });
            }
        }

        public JsonResult Sair()
        {
            try
            {
                VotoModel model = new VotoModel();
                model.AbandonarCredenciais();
                return Json(new { sucesso = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { sucesso = false, erro = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize(Roles = "Superadmin,Admin")]
        public JsonResult Excluir(VotacaoModel model)
        {
            try
            {
                bool suc = model.Excluir();
                return Json(new { sucesso = suc, id = model.ID });
            }
            catch (Exception e)
            {
                return Json(new { sucesso = false, erro = e.Message });
            }
        }
    }
}