﻿using PainelEletronicoWeb.Models;
using System;
using System.Web.Mvc;

namespace PainelEletronicoWeb.Controllers
{
    public class VotoController : Controller
    {
        [HttpPost]
        public string Registrar(string diretorioCamara,string codigoPrisma,string voto)
        {
            try
            {
                VotoModel vt = new VotoModel(diretorioCamara, codigoPrisma, voto);
                return vt.Salvar();
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }
    }
}