﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using PainelEletronicoWeb.EntityFramework;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using System.Text;

namespace PainelEletronicoWeb.Models
{
    public class CamaraMunicipalModel
    {
        public Guid ID { get; set; }

        [Display(Name = "Descrição")]
        public string Descricao { get; set; }

        [Display(Name = "Endereço")]
        public string Endereco { get; set; }

        [Display(Name = "Cidade")]
        public string Cidade { get; set; }

        [Display(Name = "Estado")]
        public string Estado { get; set; }

        [Display(Name = "Tempo uso palavra")]
        public int TempoUsoPalavra { get; set; }

        [Display(Name = "Tempo uso aparte")]
        public int TempoUsoAparte { get; set; }

        [Display(Name = "Administrador")]
        public string UsuarioAdministrador { get; set; }

        [Display(Name = "Senha")]
        public string SenhaUsuarioAdministrador { get; set; }

        [Display(Name = "Brasão")]
        public byte[] Brasao { get; set; }
        public string BrasaoBase64String { get; set; }

        [Display(Name = "Diretório")]
        public string Diretorio { get; set; }

        [Display(Name = "Presidente")]
        public string Presidente { get; set; }

        [Display(Name = "Vereadores")]
        public IEnumerable<VereadorModel> Vereadores { get; set; }

        public VotacaoModel Votacao { get; set; }

        public IEnumerable<SelectListItem> Estados;

        public CamaraMunicipalModel()
        {
            Estados = new SelectList((new EstadoModel()).Listar(), "Estado", "Estado");
        }

        public CamaraMunicipalModel(string diretorio)
        {
            try
            {
                var c = new paineleletronicoEntities();
                var s = c.camaras_municipais.FirstOrDefault(cam=>cam.Diretorio==diretorio);

                if (s!=null)
                {
                    var sessoes = c.sessoes_votacao.Where(se => se.IDCamaraMunicipal == s.ID).OrderByDescending(se => se.DataEncerramento).ToList();
                    if (sessoes.Any())
                    {
                        var sessoes_abertas = sessoes.Where(se => se.Status == "ABERTA").ToList();
                        if (sessoes_abertas.Any())
                        {
                            sessoes = sessoes_abertas.ToList();
                        }
                        this.Votacao =new VotacaoModel(sessoes.First());
                    }

                    this.ID = s.ID;
                    this.Descricao = s.Descricao;
                    this.Endereco = s.Endereco;
                    this.Cidade = s.Cidade;
                    this.Estado = s.Estado;
                    this.TempoUsoPalavra = s.TempoUsoPalavra;
                    this.TempoUsoAparte = s.TempoUsoAparte;
                    this.BrasaoBase64String = Convert.ToBase64String((s.Brasao == null) ? new byte[] { } : s.Brasao);
                    this.Diretorio = s.Diretorio;
                    this.Vereadores = new VereadorModel().Listar(this);
                    this.Presidente = s.Presidente;
                }
                else
                {
                    throw new ArgumentException("Câmara Municipal não localizada para o diretório especificado ( /" + diretorio + " )");
                }
            }
            catch
            {
                throw;
            }
        }

        public static Guid ObterID(string userName)
        {
            try
            {
                var db = new paineleletronicoEntities();
                var c = db.camaras_municipais.FirstOrDefault(cam=>cam.UsuarioAdministrador==userName);
                if (c != null)
                {
                    return c.ID;
                }
                return Guid.Empty;
            }
            catch
            {
                throw;
            }
        }

        public static string ObterDiretorio(string userName)
        {
            try
            {
                var db = new paineleletronicoEntities();
                var c = db.camaras_municipais.FirstOrDefault(cam => cam.UsuarioAdministrador == userName);
                if (c != null)
                {
                    return c.Diretorio;
                }
                return string.Empty;
            }
            catch
            {
                throw;
            }
        }

        public bool Excluir()
        {
            try
            {
                var db = new paineleletronicoEntities();
                var c = db.camaras_municipais.Where(cam=>cam.ID==this.ID).FirstOrDefault();
                if(c!=null)
                {
                    db.camaras_municipais.Remove(c);
                    db.SaveChanges();
                }
                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool Salvar(ApplicationUserManager userManager)
        {
            try
            {
                if(!string.IsNullOrEmpty(this.BrasaoBase64String))
                {
                    this.Brasao = Convert.FromBase64String(this.BrasaoBase64String);
                }
                var db = new paineleletronicoEntities();
                camaras_municipais c;
                bool novoRegistro = true;
                if (this.ID.Equals(Guid.Empty))
                {
                    this.ID = Guid.NewGuid();
                    c = db.camaras_municipais.Create();
                    c.ID = this.ID;
                }
                else
                {
                    novoRegistro = false;
                    c = db.camaras_municipais.Where(cam => cam.ID == this.ID).FirstOrDefault();
                }

                if(c!=null)
                {
                    if(RegistrarUsuario(userManager,this.UsuarioAdministrador,this.SenhaUsuarioAdministrador,this.ID))
                    {
                        c.Descricao = this.Descricao;
                        c.Endereco = this.Endereco;
                        c.Cidade = this.Cidade;
                        c.Estado = this.Estado;
                        c.TempoUsoPalavra = this.TempoUsoPalavra;
                        c.TempoUsoAparte = this.TempoUsoAparte;
                        c.UsuarioAdministrador = this.UsuarioAdministrador;
                        c.SenhaUsuarioAdministrador = this.SenhaUsuarioAdministrador;
                        c.Diretorio = this.Diretorio;
                        c.Presidente = this.Presidente;
                        if (this.Brasao != null && Brasao.Length > 0)
                            c.Brasao = this.Brasao;

                        if (novoRegistro)
                        {
                            db.camaras_municipais.Add(c);
                        }
                        else
                        {
                            db.Entry(c).State = EntityState.Modified;
                        }

                        db.SaveChanges();
                    }
                }

                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                //Garante que não vai sobrar usuário no membership sem que a câmara
                //esteja cadastrada
                var db = new paineleletronicoEntities();
                var c = db.camaras_municipais.Where(cam => cam.ID == this.ID);
                var u = userManager.FindByEmail("admin@" + this.ID.ToString());
                if (!c.Any() && u != null)
                    userManager.Delete(u);
            }
        }

        private bool RegistrarUsuario(ApplicationUserManager userManager,string usuario, string senha, Guid idCamara)
        {
            try
            {
                var db = new paineleletronicoEntities();
                var c = db.camaras_municipais.Where(cam => cam.UsuarioAdministrador == usuario && cam.ID != idCamara);
                if (c.Any())
                    throw new ArgumentException("Usuário já existente em outra câmara municipal !");

                var user = new ApplicationUser();
                user.UserName = usuario;
                user.Email = "admin@" + idCamara.ToString();

                var fuser = userManager.FindByEmail(user.Email);
                IdentityResult r = IdentityResult.Failed(string.Empty);
                if(fuser==null)
                    VerificarErros(userManager.Create(user, senha));
                else
                {
                    user.Id = fuser.Id;
                    fuser.UserName = usuario;

                    VerificarErros(userManager.RemovePassword(fuser.Id));
                    VerificarErros(userManager.AddPassword(fuser.Id, senha));
                    VerificarErros(userManager.Update(fuser));
                }

                if (!userManager.IsInRole(user.Id, "Admin"))
                    VerificarErros(userManager.AddToRole(user.Id, "Admin"));

                return true;
            }
            catch
            {
                throw;
            }
        }

        private void VerificarErros(IdentityResult r)
        {
            try
            {
                if(!r.Succeeded)
                {
                    StringBuilder erros = new StringBuilder();
                    foreach (string e in r.Errors)
                    {
                        erros.Append(e + "\n");
                    }
                    throw new ArgumentException(erros.ToString());
                }
            }
            catch
            {
                throw;
            }
        }


        public IEnumerable<CamaraMunicipalModel> Listar()
        {
            try
            {
                var c = new paineleletronicoEntities();
                var l = c.camaras_municipais.ToList();
                var retorno = l.Select(s => new CamaraMunicipalModel()
                {
                    ID = s.ID,
                    Descricao = s.Descricao,
                    Endereco = s.Endereco,
                    Cidade = s.Cidade,
                    Estado = s.Estado,
                    TempoUsoPalavra = s.TempoUsoPalavra,
                    TempoUsoAparte = s.TempoUsoAparte,
                    UsuarioAdministrador = s.UsuarioAdministrador,
                    SenhaUsuarioAdministrador = s.SenhaUsuarioAdministrador,
                    Brasao = s.Brasao,
                    BrasaoBase64String = Convert.ToBase64String((s.Brasao==null)?new byte[] { }:s.Brasao),
                    Diretorio = s.Diretorio,
                    Presidente = s.Presidente
                });
                return retorno;
            }
            catch
            {
                throw;
            }
        }
    }
}