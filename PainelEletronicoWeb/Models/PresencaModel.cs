﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PainelEletronicoWeb.EntityFramework;

namespace PainelEletronicoWeb.Models
{
    public class PresencaModel
    {
        public PresencaModel()
        {

        }
        public List<VizualizarPresencaViewModel> Presenca()
        {

            var c = new paineleletronicoEntities();
            var res = (from r in c.registro_presenca
                       join u in c.usuarios
                       on r.ID equals u.ID
                       select new VizualizarPresencaViewModel
                       {
                           Nome = u.Nome,
                           DataPresenca = r.DataPresenca,

                       }).ToList();
            return res;
        }
    }
}