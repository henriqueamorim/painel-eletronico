﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using PainelEletronicoWeb.EntityFramework;
using System.Web;
using System.Collections.Generic;

namespace PainelEletronicoWeb.Models
{
    public class VotoModel
    {
        [Display(Name = "Voto")]
        public string Voto { get; set; }

        public Guid IDSessao { get; set; }
        public Guid IDUsuario { get; set; }

        public VotoModel()
        {

        }

        public VotoModel(string diretorioCamara, string codigoPrisma, string voto)
        {
            try
            {
                if (string.IsNullOrEmpty(diretorioCamara))
                    throw new ArgumentException("DIRETORIO_CAMARA_NAO_INFORMADO");
                if (string.IsNullOrEmpty(codigoPrisma))
                    throw new ArgumentException("CODIGO_PRISMA_NAO_INFORMADO");
                if (string.IsNullOrEmpty(voto))
                    throw new ArgumentException("VOTO_NAO_INFORMADO");

                var db = new paineleletronicoEntities();
                var c = db.camaras_municipais.FirstOrDefault(cam => cam.Diretorio == diretorioCamara);
                if (c == null)
                    throw new ArgumentException("DIRETORIO_CAMARA_NAO_LOCALIZADO");
                int cPrisma = Convert.ToInt32(codigoPrisma);
                var u = db.usuarios.FirstOrDefault(usu => usu.CodigoPrisma == cPrisma && usu.IDCamaraMunicipal == c.ID);
                if (u == null)
                    throw new ArgumentException("CODIGO_PRISMA_NAO_LOCALIZADO");
                if(voto != "SIM" && voto != "NAO" && voto != "ABSTENCAO")
                    throw new ArgumentException("VOTO_INVALIDO");
                var v = db.sessoes_votacao.FirstOrDefault(vo => vo.Status == "ABERTA" && vo.IDCamaraMunicipal == c.ID);
                if(v==null)
                    throw new ArgumentException("NENHUMA_VOTACAO_ABERTA");

                this.IDSessao = v.ID;
                this.IDUsuario = u.ID;
                this.Voto = voto;               
            }
            catch
            {
                throw;
            }
        }

        public VotoModel(Guid idVereador, string senha)
        {
            try
            {
                var db = new paineleletronicoEntities();
                var ver = db.usuarios.FirstOrDefault(v => v.ID == idVereador);
                if (ver == null)
                    throw new ArgumentException("Vereador não encontrado");
                else
                {
                    if (!ver.Ativo.Equals("1"))
                        throw new ArgumentException("Vereador não está ativo");
                }
                if (!ver.Senha.Equals(senha))
                    throw new ArgumentException("Senha incorreta");

                var s = db.sessoes_votacao.FirstOrDefault(vo => vo.Status == "ABERTA" && vo.IDCamaraMunicipal == ver.IDCamaraMunicipal);
                if (s == null)
                    throw new ArgumentException("Nenhuma votação aberta no momento !");

                HttpCookie authCookie = HttpContext.Current.Request.Cookies["Autenticacao"];

                if(authCookie != null)
                {
                    authCookie.Values.Add("IDVereador", ver.ID.ToString());
                    authCookie.Values.Add("NomeVereador", ver.Nome);
                    authCookie.Expires = DateTime.Now.AddYears(1);

                    HttpContext.Current.Response.Cookies.Set(authCookie);
                }
                else
                {
                    authCookie = new HttpCookie("Autenticacao");
                    authCookie.Values.Add("IDVereador", ver.ID.ToString());
                    authCookie.Values.Add("NomeVereador", ver.Nome);
                    authCookie.Expires = DateTime.Now.AddYears(1);

                    HttpContext.Current.Response.Cookies.Add(authCookie);
                }

                this.IDUsuario = ver.ID;
                this.IDSessao = s.ID;

            }
            catch
            {
                throw;
            }
        }

        public VotoModel(Guid idVereador, string senha, string voto)
        {
            try
            {
                var db = new paineleletronicoEntities();
                var ver = db.usuarios.FirstOrDefault(v => v.ID == idVereador);
                if (ver == null)
                    throw new ArgumentException("Vereador não encontrado");
                else
                {
                    if(!ver.Ativo.Equals("1"))
                        throw new ArgumentException("Vereador não está ativo");
                }

                VereadorModel ve = ObterCredenciais((Guid)ver.IDCamaraMunicipal);          
                if(ve==null || !ve.ID.Equals(idVereador))       
                    throw new ArgumentException("Vereador não autenticado !");

                var s = db.sessoes_votacao.FirstOrDefault(vo => vo.Status == "ABERTA" && vo.IDCamaraMunicipal == ver.IDCamaraMunicipal);
                if (s == null)
                    throw new ArgumentException("Nenhuma votação aberta no momento !");

                var vt = db.votos.FirstOrDefault(vo=>vo.IDSessao==s.ID && vo.IDUsuario == ver.ID);
                if (vt != null)
                {
                    db.votos.Remove(vt);
                    db.SaveChanges();
                }

                this.IDSessao = s.ID;
                this.IDUsuario = ver.ID;
                this.Voto = voto;
            }
            catch
            {
                throw;
            }
        }

        public void RegistrarPresenca()
        {
            try
            {
                var db = new paineleletronicoEntities();

                //Se o vereador ainda não tem nenhum voto registrado na sessão
                var vt = db.votos.FirstOrDefault(vo => vo.IDSessao == this.IDSessao && vo.IDUsuario == this.IDUsuario);
                if (vt == null)
                {
                    //Registra o vereador como presente
                    votos c = db.votos.Create();

                    c.IDSessao = this.IDSessao;
                    c.IDUsuario = this.IDUsuario;
                    c.voto = "PRESENTE";

                    db.votos.Add(c);

                    db.SaveChanges();
                }
            }
            catch
            {
                throw;
            }
        }

        public void AbandonarCredenciais()
        {
            try
            {
                HttpCookie currentUserCookie = HttpContext.Current.Request.Cookies["Autenticacao"];
                HttpContext.Current.Response.Cookies.Remove("Autenticacao");
                currentUserCookie.Expires = DateTime.Now.AddDays(-10);
                currentUserCookie.Value = null;
                HttpContext.Current.Response.SetCookie(currentUserCookie);
            }
            catch
            {
                throw;
            }
        }

        public VereadorModel ObterCredenciais(Guid idCamara)
        {
            try
            {
                VereadorModel v = null;

                HttpCookie authCookie = new HttpCookie("Autenticacao");
                authCookie = HttpContext.Current.Request.Cookies["Autenticacao"];

                if(authCookie != null)
                {
                    Guid idVereador = new Guid(authCookie.Values["IDVereador"].ToString());

                    var db = new paineleletronicoEntities();
                    var ver = db.usuarios.FirstOrDefault(ve => ve.ID == idVereador && ve.IDCamaraMunicipal == idCamara);

                    if(ver!=null)
                    {
                        v = new VereadorModel();
                        v.ID = ver.ID;
                        v.Nome = ver.Nome;
                    }
                }
                return v;
            }
            catch
            {
                throw;
            }
        }

        public List<VisualizarVotacaoViewModel> TrazerVotos()
        {
           
            var c = new paineleletronicoEntities();
            var res = (from v in c.votos
                      join u in c.usuarios
                      on v.IDUsuario equals u.ID
                      join sv in c.sessoes_votacao
                      on v.IDSessao equals sv.ID
                      where sv.Status == "ABERTA"

                      select new VisualizarVotacaoViewModel
                      {
                          Nome = u.Nome,
                          Voto = v.voto,
                          Descricao = sv.Descricao
                      }).ToList();
            return res;
        }

       
        public string Salvar()
        {
            try
            {
                var db = new paineleletronicoEntities();
                votos c = db.votos.Create();

                c.IDSessao = this.IDSessao;
                c.IDUsuario = this.IDUsuario;
                c.voto = this.Voto;

                db.votos.Add(c);

                db.SaveChanges();

                return "SUCESSO";
            }
            catch
            {
                throw;
            }
        }
    }
}
