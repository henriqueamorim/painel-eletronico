﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PainelEletronicoWeb.Models
{
    public class VisualizarVotacaoViewModel
    {
        public string Nome { get; set; }
        public string Voto { get; set; }
        public string Descricao { get; set; }
    }
}