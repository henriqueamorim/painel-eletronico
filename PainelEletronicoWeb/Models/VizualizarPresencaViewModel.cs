﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PainelEletronicoWeb.Models
{
    public class VizualizarPresencaViewModel
    {
        public string Nome { get; set; }

        public DateTime DataPresenca { get; set; }
    }
}