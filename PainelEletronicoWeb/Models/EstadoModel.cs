﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Collections.Generic;

namespace PainelEletronicoWeb.Models
{
    public class EstadoModel
    {
        [Display(Name = "Estado")]
        public string Estado { get; set; }

        private List<EstadoModel> _Estados;

        public IEnumerable<EstadoModel> Listar()
        {
            if (_Estados == null || _Estados.Count == 0)
                CarregarEstados();
            return _Estados.ToList();
        }

        private void CarregarEstados()
        {
            _Estados = new List<EstadoModel>();
            _Estados.Add(new EstadoModel() { Estado = "AC" });
            _Estados.Add(new EstadoModel() { Estado = "AL" });
            _Estados.Add(new EstadoModel() { Estado = "AM" });
            _Estados.Add(new EstadoModel() { Estado = "AP" });
            _Estados.Add(new EstadoModel() { Estado = "BA" });
            _Estados.Add(new EstadoModel() { Estado = "CE" });
            _Estados.Add(new EstadoModel() { Estado = "DF" });
            _Estados.Add(new EstadoModel() { Estado = "ES" });
            _Estados.Add(new EstadoModel() { Estado = "GO" });
            _Estados.Add(new EstadoModel() { Estado = "MA" });
            _Estados.Add(new EstadoModel() { Estado = "MG" });
            _Estados.Add(new EstadoModel() { Estado = "MS" });
            _Estados.Add(new EstadoModel() { Estado = "MT" });
            _Estados.Add(new EstadoModel() { Estado = "PA" });
            _Estados.Add(new EstadoModel() { Estado = "PB" });
            _Estados.Add(new EstadoModel() { Estado = "PE" });
            _Estados.Add(new EstadoModel() { Estado = "PI" });
            _Estados.Add(new EstadoModel() { Estado = "PR" });
            _Estados.Add(new EstadoModel() { Estado = "RJ" });
            _Estados.Add(new EstadoModel() { Estado = "RN" });
            _Estados.Add(new EstadoModel() { Estado = "RS" });
            _Estados.Add(new EstadoModel() { Estado = "RO" });
            _Estados.Add(new EstadoModel() { Estado = "RR" });
            _Estados.Add(new EstadoModel() { Estado = "SC" });
            _Estados.Add(new EstadoModel() { Estado = "SE" });
            _Estados.Add(new EstadoModel() { Estado = "SP" });
            _Estados.Add(new EstadoModel() { Estado = "TO" });
        }
    }
}