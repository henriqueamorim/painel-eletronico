﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using PainelEletronicoWeb.EntityFramework;
using System.Collections.Generic;
using System.Data.Entity;
using System.Web.Mvc;

namespace PainelEletronicoWeb.Models
{
    public class VotacaoModel
    {
        public Guid ID { get; set; }

        [Display(Name = "Descrição")]
        public string Descricao { get; set; }

        [Display(Name = "Data da votação")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DataVotacao { get; set; }

        [Display(Name = "Status")]
        public string Status { get; set; }

        [Display(Name = "Opções de voto")]
        public string OpcoesVoto { get; set; }

        [Display(Name = "Data da inclusão")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DataInclusao { get; set; }

        [Display(Name = "Data de encerramento")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? DataEncerramento { get; set; }

        [Display(Name = "Data de abertura")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? DataAbertura { get; set; }

        [Display(Name = "Resultado da votação")]
        public string ResultadoVotacao { get; set; }

        [Display(Name = "Câmara Municipal")]
        public Guid IDCamaraMunicipal { get; set; }

        public int TotalPresentes { get; set; }
        public int TotalAusentes { get; set; }
        public int TotalGeral { get; set; }
        public int TotalSim { get; set; }
        public int TotalNao { get; set; }
        public int TotalAbstencao { get; set; }
        public string[] Opcoes { get; set; }

        public IEnumerable<SelectListItem> CamarasMunicipais;
        public IEnumerable<SelectListItem> ListaStatus;
        public IEnumerable<SelectListItem> ListaResultado;

        public VotacaoModel(sessoes_votacao sessao)
        {
            this.ID = sessao.ID;
            this.IDCamaraMunicipal = sessao.IDCamaraMunicipal;
            this.Status = sessao.Status;
            this.OpcoesVoto = sessao.OpcoesVoto;
            this.DataAbertura = sessao.DataAbertura;
            this.DataEncerramento = sessao.DataEncerramento;
            this.DataInclusao = sessao.DataInclusao;
            this.DataVotacao = sessao.DataVotacao;
            this.Descricao = sessao.Descricao;
            this.ResultadoVotacao = sessao.ResultadoVotacao;
            this.Opcoes = this.OpcoesVoto.Split('/');

            int totalGeral = 0;
            int totalPresentes = 0;
            int totalAusentes = 0;
            int totalSim = 0;
            int totalNao = 0;
            int totalAbstencao = 0;
            string resultadoVotacao = string.Empty;

            ObterTotais(this.ID,ref totalGeral, ref totalPresentes,ref totalAusentes,ref totalSim,ref totalNao,ref totalAbstencao,ref resultadoVotacao);

            this.TotalGeral = TotalGeral;
            this.TotalPresentes = totalPresentes;
            this.TotalAusentes = totalAusentes;
            this.TotalSim = totalSim;
            this.TotalNao = totalNao;
            this.TotalAbstencao = totalAbstencao;
            this.ResultadoVotacao = resultadoVotacao;

        }

        public static void ObterTotais(Guid idSessao,ref int totalGeral,ref int totalPresentes,ref int totalAusentes,ref int totalSim,ref int totalNao,ref int totalAbstencao,ref string resultadoVotacao)
        {
            try
            {
                var db = new paineleletronicoEntities();
                var sessao = db.sessoes_votacao.FirstOrDefault(s=>s.ID==idSessao);
                var totais = from vot in db.votos.ToList() join
                             ses in db.sessoes_votacao.ToList() on vot.IDSessao equals ses.ID
                             where ses.ID == idSessao
                             select new { vot.voto };

                var totalVereadores = db.usuarios.Where(u => u.IDCamaraMunicipal == sessao.IDCamaraMunicipal && u.Ativo=="1").Count();

                totalGeral = totalVereadores;
                totalPresentes = totais.Count();
                totalAusentes = totalGeral - totalPresentes;
                totalSim = totais.Where(t => t.voto == "SIM").Count();
                totalNao = totais.Where(t => t.voto == "NAO").Count();
                totalAbstencao = totais.Where(t => t.voto == "ABSTENCAO").Count();
                resultadoVotacao = sessao.ResultadoVotacao;
            }
            catch
            {
                throw;
            }
        }

        public VotacaoModel()
        {
            var db = new paineleletronicoEntities();
            CamarasMunicipais = new SelectList(db.camaras_municipais.ToList().OrderBy(p => p.Descricao), "ID", "Descricao");
            Dictionary<string, string> lstStatus = new Dictionary<string, string>();
            lstStatus.Add("AGENDADA", "AGENDADA");
            lstStatus.Add("ABERTA", "ABERTA");
            lstStatus.Add("ENCERRADA", "ENCERRADA");
            ListaStatus = new SelectList(lstStatus, "Key", "Value");
            Dictionary<string, string> lstResultado = new Dictionary<string, string>();
            lstResultado.Add("INDEFINIDO", "INDEFINIDO");
            lstResultado.Add("APROVADO", "APROVADO");
            lstResultado.Add("REPROVADO", "REPROVADO");
            ListaResultado = new SelectList(lstResultado, "Key", "Value");
        }

        public bool Excluir()
        {
            try
            {
                var db = new paineleletronicoEntities();
                var c = db.sessoes_votacao.FirstOrDefault(cam => cam.ID == this.ID);
                if (c != null)
                {
                    db.sessoes_votacao.Remove(c);
                    db.SaveChanges();
                }
                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool Salvar()
        {
            try
            {
                var db = new paineleletronicoEntities();
                sessoes_votacao c;
                bool novoRegistro = true;
                if (this.ID.Equals(Guid.Empty))
                {
                    this.ID = Guid.NewGuid();
                    c = db.sessoes_votacao.Create();
                    c.ID = this.ID;
                }
                else
                {
                    novoRegistro = false;
                    c = db.sessoes_votacao.FirstOrDefault(cam => cam.ID == this.ID);
                }

                if(this.Status.Equals("ABERTA"))
                {
                    var abertas = db.sessoes_votacao.Where(cam => cam.ID != this.ID && cam.Status == "ABERTA" && cam.IDCamaraMunicipal == this.IDCamaraMunicipal);
                    if (abertas.Any())
                        throw new ArgumentException("Já existe uma votação ABERTA. Escolha outro Status ou mude o Status da votação existente e tente novamente !");
                }

                if (c != null)
                {
                    c.Descricao = this.Descricao;
                    c.DataVotacao = this.DataVotacao;
                    c.Status = this.Status;
                    c.OpcoesVoto = this.OpcoesVoto;
                    c.DataInclusao = this.DataInclusao;
                    c.DataEncerramento = this.DataEncerramento;
                    c.DataAbertura = this.DataAbertura;
                    c.ResultadoVotacao = this.ResultadoVotacao;
                    c.IDCamaraMunicipal = this.IDCamaraMunicipal;

                    if (novoRegistro)
                    {
                        db.sessoes_votacao.Add(c);
                    }
                    else
                    {
                        db.Entry(c).State = EntityState.Modified;
                    }

                    db.SaveChanges();
                }

                return true;
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<VotacaoModel> Listar()
        {
            try
            {
                return Listar(Guid.Empty);
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<VotacaoModel> Listar(Guid idCamara)
        {
            try
            {                
                var c = new paineleletronicoEntities();
                var l = c.sessoes_votacao.ToList().Where(cam=>cam.IDCamaraMunicipal==idCamara || idCamara == Guid.Empty);
                var retorno = l.Select(s => new VotacaoModel()
                {
                    ID = s.ID,
                    Descricao = s.Descricao,
                    DataVotacao = s.DataVotacao,
                    Status = s.Status,
                    OpcoesVoto = s.OpcoesVoto,
                    DataInclusao = s.DataInclusao,
                    DataEncerramento = s.DataEncerramento,
                    DataAbertura = s.DataAbertura,
                    ResultadoVotacao = s.ResultadoVotacao,
                    IDCamaraMunicipal = s.IDCamaraMunicipal
                });
                return retorno;
            }
            catch
            {
                throw;
            }
        }

        public static string OpcaoEscolhida(string voto, string[] opcoes)
        {
            try
            {
                if(opcoes!=null && opcoes.Length>=3)
                {
                    switch (voto.Trim())
                    {
                        case "SIM":
                            return opcoes[0].Trim();
                        case "NAO":
                            return opcoes[1].Trim();
                        case "ABSTENCAO":
                            return opcoes[2].Trim();
                        case "PRESENTE":
                        case "AUSENTE":
                            return voto;
                        default:
                            return string.Empty;
                    }
                }
                else
                {
                    return string.Empty;
                }
            }
            catch
            {
                throw;
            }
        }
    }
}
