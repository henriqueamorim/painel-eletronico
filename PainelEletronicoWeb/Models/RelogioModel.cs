﻿using PainelEletronicoWeb.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PainelEletronicoWeb.Models
{
    public class RelogioModel
    {
        public static void Obter(Guid idCamara, string tipoRelogio, ref string relogio)
        {
            try
            {
                var db = new paineleletronicoEntities();
                var rel = db.relogios.FirstOrDefault(s => s.IDCamaraMunicipal == idCamara && s.Tipo == tipoRelogio);
                if (rel == null)
                {
                    relogio = string.Format(relogio, "00", "00", "00");
                }
                else
                {
                    TimeSpan? dif = rel.DataHoraFim - DateTime.Now;
                    if (rel.Status.Equals("PAUSADO"))
                        dif = rel.DataHoraFim - rel.DataHoraInicio;
                    if (dif.Value.Hours <= 0 && dif.Value.Minutes <= 0 && dif.Value.Seconds <= 0)
                    {
                        Parar(idCamara, tipoRelogio);
                        relogio = string.Format(relogio, "00", "00", "00");
                    }
                    else
                    {
                        relogio = string.Format(relogio, dif.Value.Hours.ToString().PadLeft(2, '0'), dif.Value.Minutes.ToString().PadLeft(2, '0'), dif.Value.Seconds.ToString().PadLeft(2, '0'));
                    }
                }
            }
            catch
            {
                throw;
            }

        }

        public static string IniciarPausar(Guid idCamara, string tipoRelogio,int intervalo,string botao)
        {
            try
            {
                string retorno = string.Empty;
                var db = new paineleletronicoEntities();
                relogios r;
                bool novoRegistro = true;
                var rel = db.relogios.FirstOrDefault(s => s.IDCamaraMunicipal == idCamara && s.Tipo == tipoRelogio);
                if (rel == null)
                {
                    r = db.relogios.Create();
                    if (botao.Equals("play"))
                    {
                        r.ID = Guid.NewGuid();
                        r.DataHoraFim = DateTime.Now.AddMinutes(intervalo);
                        r.IDCamaraMunicipal = idCamara;
                        r.Status = "EXECUTANDO";
                        r.Tipo = tipoRelogio;
                    }
                }
                else
                {
                    novoRegistro = false;
                    r = rel;
                    if (r.Status.Equals("PAUSADO") && botao.Equals("play"))
                    {
                        r.Status = "EXECUTANDO";
                        TimeSpan? dif = rel.DataHoraFim - rel.DataHoraInicio;
                        r.DataHoraFim = DateTime.Now.Add((TimeSpan)dif);
                    }
                    else if (r.Status.Equals("EXECUTANDO") && botao.Equals("pause"))
                    {
                        r.Status = "PAUSADO";
                        r.DataHoraInicio = DateTime.Now;
                    }
                }

                retorno = r.Status;

                if (!string.IsNullOrEmpty(retorno))
                {
                    if (novoRegistro)
                    {
                        db.relogios.Add(r);
                    }
                    else
                    {
                        db.Entry(r).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                }

                return retorno;
            }
            catch
            {
                throw;
            }
        }

        public static string Parar(Guid idCamara,string tipo)
        {
            try
            {
                var db = new paineleletronicoEntities();
                var r = db.relogios.Where(rel => rel.IDCamaraMunicipal == idCamara && rel.Tipo == tipo);
                db.relogios.RemoveRange(r);
                db.SaveChanges();
                return "PARADO";
            }
            catch
            {
                throw;
            }
        }
    }
}