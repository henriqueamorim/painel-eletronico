﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using PainelEletronicoWeb.EntityFramework;
using System.Collections.Generic;
using System.Data.Entity;
using System.Web.Mvc;

namespace PainelEletronicoWeb.Models
{
    public class VereadorModel
    {
        public Guid ID { get; set; }

        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Display(Name = "E-Mail")]
        public string Email { get; set; }

        [Display(Name = "Ativo")]
        public bool Ativo { get; set; }

        [Display(Name = "Senha")]
        public string Senha { get; set; }

        [Display(Name = "Código prisma")]
        public int? CodigoPrisma { get; set; }

        [Display(Name = "Partido")]
        public Guid? IDPartido { get; set; }

        [Display(Name = "Câmara Municipal")]
        public Guid? IDCamaraMunicipal { get; set; }

        [Display(Name = "Voto")]
        public string Voto { get; set; }

        [Display(Name = "Foto")]
        public byte[] Foto { get; set; }
        public string FotoBase64String { get; set; }

        public string RedefinirSenha { get; set; }
        public string SiglaPartido { get; set; }

        public Guid IDPerfil { get; set; }

        public IEnumerable<SelectListItem> Partidos;
        public IEnumerable<SelectListItem> CamarasMunicipais;

        public VereadorModel()
        {
            var db = new paineleletronicoEntities();
            Partidos = new SelectList(db.partidos.ToList().OrderBy(p => p.Sigla), "ID", "Sigla");
            CamarasMunicipais = new SelectList(db.camaras_municipais.ToList().OrderBy(p => p.Descricao), "ID", "Descricao");
        }

        public bool Excluir()
        {
            try
            {
                var db = new paineleletronicoEntities();
                var c = db.usuarios.Where(cam => cam.ID == this.ID).FirstOrDefault();
                if (c != null)
                {
                    db.usuarios.Remove(c);
                    db.SaveChanges();
                }
                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool Salvar()
        {
            try
            {
                if (!string.IsNullOrEmpty(this.FotoBase64String))
                {
                    this.Foto = Convert.FromBase64String(this.FotoBase64String);
                }
                var db = new paineleletronicoEntities();
                usuarios c;
                bool novoRegistro = true;
                if (this.ID.Equals(Guid.Empty))
                {
                    this.ID = Guid.NewGuid();
                    c = db.usuarios.Create();
                    c.ID = this.ID;
                }
                else
                {
                    novoRegistro = false;
                    c = db.usuarios.Where(cam => cam.ID == this.ID).FirstOrDefault();
                }

                if (c != null)
                {
                    c.Nome = this.Nome;
                    c.Email = this.Email;
                    c.Ativo = (this.Ativo)?"1":string.Empty;
                    c.Senha = this.Senha;
                    c.IDPartido = this.IDPartido;
                    c.IDPerfil = db.perfis.FirstOrDefault(p => p.Perfil == "Vereador").ID;
                    if (this.Foto != null && Foto.Length > 0)
                        c.Foto = this.Foto;
                    c.RedefinirSenha = this.RedefinirSenha;
                    c.IDCamaraMunicipal = this.IDCamaraMunicipal;
                    c.CodigoPrisma = this.CodigoPrisma;

                    if (novoRegistro)
                    {
                        db.usuarios.Add(c);
                    }
                    else
                    {
                        db.Entry(c).State = EntityState.Modified;
                    }

                    db.SaveChanges();
                }

                return true;
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<VereadorModel> Listar()
        {
            try
            {
                return Vereadores(Guid.Empty);
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<VereadorModel> Listar(Guid idCamara)
        {
            try
            {
                return Vereadores(idCamara);
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<VereadorModel> Listar(CamaraMunicipalModel camara)
        {
            try
            {
                var c = new paineleletronicoEntities();
                if(camara == null)
                {
                    return Vereadores(Guid.Empty);
                }
                else
                {
                    return VereadoresCamara(camara);
                }
            }
            catch
            {
                throw;
            }
        }

        private IEnumerable<VereadorModel> Vereadores(Guid idCamara)
        {
            try
            {
                var c = new paineleletronicoEntities();



                //var res = from v in c.votos
                //          join u in c.usuarios
                //          on v.IDUsuario equals u.ID
                //          join sv in c.sessoes_votacao
                //          on v.IDSessao equals sv.ID
                //          where sv.Status == "ABERTO"

                //          select new
                //          {
                //              Nome = u.Nome,
                //              Voto = v.voto,
                //              Descricao = sv.Descricao
                //          };

                var l = c.usuarios.Where(usu=>usu.IDCamaraMunicipal == idCamara || idCamara.Equals(Guid.Empty)).ToList();
                var retorno = l.Select(s => new VereadorModel()
                {
                    ID = s.ID,
                    Nome = s.Nome,
                    Email = s.Email,
                    Ativo = (s.Ativo=="1"),
                    Senha = s.Senha,
                    IDPartido = s.IDPartido,
                    IDPerfil = s.IDPerfil,
                    Foto = s.Foto,
                    FotoBase64String = Convert.ToBase64String((s.Foto == null) ? new byte[] { } : s.Foto),
                    RedefinirSenha = s.RedefinirSenha,
                    IDCamaraMunicipal = s.IDCamaraMunicipal,
                    CodigoPrisma = s.CodigoPrisma
                });
                return retorno;
            }
            catch
            {
                throw;
            }
        }

        private IEnumerable<VereadorModel> VereadoresCamara(CamaraMunicipalModel camara)
        {
            try
            {
                var c = new paineleletronicoEntities();
                string[] opcoesVoto = new string[] { };
                IEnumerable<votos> votos_sessao = new List<votos>();
                if(camara.Votacao!=null)
                {
                    opcoesVoto = camara.Votacao.OpcoesVoto.Split('/');
                    votos_sessao = c.votos.Where(vo => vo.IDSessao == camara.Votacao.ID).ToList();
                }
                var vereadores_camara = from ver in c.usuarios.ToList() join
                                        par in c.partidos.ToList() on ver.IDPartido equals par.ID join
                                        vot in votos_sessao on ver.ID equals vot.IDUsuario into vereadores
                                        from v2 in vereadores.DefaultIfEmpty()
                                        where
                                            ver.IDCamaraMunicipal == camara.ID && ver.Ativo == "1"
                                        select new { SiglaPartido = par.Sigla,Foto = ver.Foto,ID = ver.ID,Nome=ver.Nome,Voto=(v2==null || v2.voto==null?"AUSENTE":v2.voto )};                                        

                var retorno = vereadores_camara.Select(s => new VereadorModel()
                {
                    ID = s.ID,
                    Nome = s.Nome,
                    Foto = s.Foto,
                    FotoBase64String = Convert.ToBase64String((s.Foto == null) ? new byte[] { } : s.Foto),
                    SiglaPartido = s.SiglaPartido,
                    Voto = VotacaoModel.OpcaoEscolhida(s.Voto,opcoesVoto)
                });
                return retorno;
            }
            catch
            {
                throw;
            }
        }
    }
}
