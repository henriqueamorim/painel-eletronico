﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PainelEletronicoWeb.Startup))]
namespace PainelEletronicoWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
