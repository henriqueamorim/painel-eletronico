-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: paineleletronico
-- ------------------------------------------------------
-- Server version	5.7.10-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `camaras_municipais`
--

DROP TABLE IF EXISTS `camaras_municipais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `camaras_municipais` (
  `ID` char(36) COLLATE latin1_general_ci NOT NULL,
  `Descricao` varchar(500) COLLATE latin1_general_ci NOT NULL,
  `Endereco` varchar(500) COLLATE latin1_general_ci NOT NULL,
  `Cidade` varchar(500) COLLATE latin1_general_ci NOT NULL,
  `Estado` char(2) COLLATE latin1_general_ci NOT NULL,
  `TempoUsoPalavra` int(11) NOT NULL,
  `TempoUsoAparte` int(11) NOT NULL,
  `UsuarioAdministrador` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `SenhaUsuarioAdministrador` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `Brasao` longblob NOT NULL,
  `Diretorio` varchar(45) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `camaras_municipais`
--

LOCK TABLES `camaras_municipais` WRITE;
/*!40000 ALTER TABLE `camaras_municipais` DISABLE KEYS */;
/*!40000 ALTER TABLE `camaras_municipais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partidos`
--

DROP TABLE IF EXISTS `partidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partidos` (
  `ID` char(36) COLLATE latin1_general_ci NOT NULL,
  `Sigla` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `Nome` varchar(200) COLLATE latin1_general_ci NOT NULL,
  `Numero` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  UNIQUE KEY `Partido_UNIQUE` (`Sigla`),
  UNIQUE KEY `Nome_UNIQUE` (`Nome`),
  UNIQUE KEY `Numero_UNIQUE` (`Numero`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partidos`
--

LOCK TABLES `partidos` WRITE;
/*!40000 ALTER TABLE `partidos` DISABLE KEYS */;
INSERT INTO `partidos` VALUES ('a532ab56-b4b1-11e5-964f-829e93055fcd','PMDB','PARTIDO DO MOVIMENTO DEMOCRÁTICO BRASILEIRO',15),('a53f1a8c-b4b1-11e5-964f-829e93055fcd','PTB','PARTIDO TRABALHISTA BRASILEIRO',14),('a545e645-b4b1-11e5-964f-829e93055fcd','PDT','PARTIDO DEMOCRÁTICO TRABALHISTA',12),('d085d09e-b4b1-11e5-964f-829e93055fcd','PT','PARTIDO DOS TRABALHADORES',13),('d095bee1-b4b1-11e5-964f-829e93055fcd','DEM','DEMOCRATAS',25),('d09e55cc-b4b1-11e5-964f-829e93055fcd','PCdoB','PARTIDO COMUNISTA DO BRASIL',65),('d0a6db40-b4b1-11e5-964f-829e93055fcd','PSB','PARTIDO SOCIALISTA BRASILEIRO',40),('d0af0915-b4b1-11e5-964f-829e93055fcd','PSDB','PARTIDO DA SOCIAL DEMOCRACIA BRASILEIRA',45),('d0b896a5-b4b1-11e5-964f-829e93055fcd','PTC','PARTIDO TRABALHISTA CRISTÃO',36),('d0bfefff-b4b1-11e5-964f-829e93055fcd','PSC','PARTIDO SOCIAL CRISTÃO',20),('d0c9fc39-b4b1-11e5-964f-829e93055fcd','PMN','PARTIDO DA MOBILIZAÇÃO NACIONAL',33),('d0d2f56e-b4b1-11e5-964f-829e93055fcd','PRP','PARTIDO REPUBLICANO PROGRESSISTA',44),('d0d95d52-b4b1-11e5-964f-829e93055fcd','PPS','PARTIDO POPULAR SOCIALISTA',23),('d0e08317-b4b1-11e5-964f-829e93055fcd','PV','PARTIDO VERDE',43),('d0e6e369-b4b1-11e5-964f-829e93055fcd','PTdoB','PARTIDO TRABALHISTA DO BRASIL',70),('d0f25774-b4b1-11e5-964f-829e93055fcd','PP','PARTIDO PROGRESSISTA',11),('d0fc31fd-b4b1-11e5-964f-829e93055fcd','PSTU','PARTIDO SOCIALISTA DOS TRABALHADORES UNIFICADO',16),('d103d611-b4b1-11e5-964f-829e93055fcd','PCB','PARTIDO COMUNISTA BRASILEIRO',21),('d10c7fbc-b4b1-11e5-964f-829e93055fcd','PRTB','PARTIDO RENOVADOR TRABALHISTA BRASILEIRO',28),('d114b7b4-b4b1-11e5-964f-829e93055fcd','PHS','PARTIDO HUMANISTA DA SOLIDARIEDADE',31),('d11d5d90-b4b1-11e5-964f-829e93055fcd','PSDC','PARTIDO SOCIAL DEMOCRATA CRISTÃO',27),('d124087f-b4b1-11e5-964f-829e93055fcd','PCO','PARTIDO DA CAUSA OPERÁRIA',29),('d12b0760-b4b1-11e5-964f-829e93055fcd','PTN','PARTIDO TRABALHISTA NACIONAL',19),('d13442d2-b4b1-11e5-964f-829e93055fcd','PSL','PARTIDO SOCIAL LIBERAL',17),('d13f6161-b4b1-11e5-964f-829e93055fcd','PRB','PARTIDO REPUBLICANO BRASILEIRO',10),('d14a2846-b4b1-11e5-964f-829e93055fcd','PSOL','PARTIDO SOCIALISMO E LIBERDADE',50),('d1531d2e-b4b1-11e5-964f-829e93055fcd','PR','PARTIDO DA REPÚBLICA',22),('d15cf7ce-b4b1-11e5-964f-829e93055fcd','PSD','PARTIDO SOCIAL DEMOCRÁTICO',55),('d1648234-b4b1-11e5-964f-829e93055fcd','PPL','PARTIDO PÁTRIA LIVRE',54),('d16b8740-b4b1-11e5-964f-829e93055fcd','PEN','PARTIDO ECOLÓGICO NACIONAL',51),('d1736956-b4b1-11e5-964f-829e93055fcd','PROS','PARTIDO REPUBLICANO DA ORDEM SOCIAL',90),('d17b9e1e-b4b1-11e5-964f-829e93055fcd','SD','SOLIDARIEDADE',77),('d1856d73-b4b1-11e5-964f-829e93055fcd','NOVO','PARTIDO NOVO',30),('d18cda50-b4b1-11e5-964f-829e93055fcd','REDE','REDE SUSTENTABILIDADE',18),('d1984bba-b4b1-11e5-964f-829e93055fcd','PMB','PARTIDO DA MULHER BRASILEIRA',35);
/*!40000 ALTER TABLE `partidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfis`
--

DROP TABLE IF EXISTS `perfis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfis` (
  `ID` char(36) COLLATE latin1_general_ci NOT NULL,
  `Perfil` varchar(100) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  UNIQUE KEY `Perfil_UNIQUE` (`Perfil`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfis`
--

LOCK TABLES `perfis` WRITE;
/*!40000 ALTER TABLE `perfis` DISABLE KEYS */;
INSERT INTO `perfis` VALUES ('e90e81bb-b4ae-11e5-964f-829e93055fcd','Administrador'),('e92c219a-b4ae-11e5-964f-829e93055fcd','Presidente da câmara'),('e9240a60-b4ae-11e5-964f-829e93055fcd','Secretário'),('e91bc78a-b4ae-11e5-964f-829e93055fcd','Vereador');
/*!40000 ALTER TABLE `perfis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessoes_votacao`
--

DROP TABLE IF EXISTS `sessoes_votacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessoes_votacao` (
  `ID` char(36) COLLATE latin1_general_ci NOT NULL,
  `Descricao` varchar(300) COLLATE latin1_general_ci NOT NULL,
  `DataVotacao` date NOT NULL,
  `Status` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `OpcoesVoto` varchar(500) COLLATE latin1_general_ci NOT NULL,
  `DataInclusao` datetime NOT NULL,
  `DataEncerramento` datetime DEFAULT NULL,
  `DataAbertura` datetime DEFAULT NULL,
  `ResultadoVotacao` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `IDCamaraMunicipal` char(36) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  UNIQUE KEY `Descricao_UNIQUE` (`Descricao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessoes_votacao`
--

LOCK TABLES `sessoes_votacao` WRITE;
/*!40000 ALTER TABLE `sessoes_votacao` DISABLE KEYS */;
INSERT INTO `sessoes_votacao` VALUES ('5947fcd9-f48f-4e88-acb6-6f2e9b1b6904','Cidade Limpa','2017-04-20','E','SIM / NÃO / ABSTENÇÃO','2017-04-20 13:29:47','2017-04-20 13:30:59','2017-04-20 13:29:52','A','');
/*!40000 ALTER TABLE `sessoes_votacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `ID` char(36) COLLATE latin1_general_ci NOT NULL,
  `Nome` varchar(200) COLLATE latin1_general_ci NOT NULL,
  `Email` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `Ativo` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `Senha` varchar(32) COLLATE latin1_general_ci NOT NULL,
  `IDPartido` char(36) COLLATE latin1_general_ci DEFAULT NULL,
  `IDPerfil` char(36) COLLATE latin1_general_ci NOT NULL,
  `Foto` longblob,
  `RedefinirSenha` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `IDCamaraMunicipal` char(36) COLLATE latin1_general_ci DEFAULT NULL,
  `CodigoPrisma` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `idUsuarios_UNIQUE` (`ID`),
  KEY `ID_idx` (`IDPerfil`),
  KEY `ID_idx1` (`IDPartido`),
  CONSTRAINT `partidos_usuarios` FOREIGN KEY (`IDPartido`) REFERENCES `partidos` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `perfis_usuarios` FOREIGN KEY (`IDPerfil`) REFERENCES `perfis` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES ('0b4d110d-301e-42e8-8c11-aade18d5a8a8','Chaves Toledo','chaves@chaves.com','1','jvom387RFVi8QkHdr9GY0w==','d09e55cc-b4b1-11e5-964f-829e93055fcd','e91bc78a-b4ae-11e5-964f-829e93055fcd','�\��\�\0JFIF\0\0`\0`\0\0�\�\0C\0		\n\r\Z\Z $.\' \",#(7),01444\'9=82<.342�\�\0C			\r\r2!!22222222222222222222222222222222222222222222222222��\0\0A\0A\"\0�\�\0\0\0\0\0\0\0\0\0\0\0	\n�\�\0�\0\0\0}\0!1AQa\"q2���#B��R\��$3br�	\n\Z%&\'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz�����������������������������������\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\������������\�\0\0\0\0\0\0\0\0	\n�\�\0�\0\0w\0!1AQaq\"2�B����	#3R�br\�\n$4\�%�\Z&\'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz������������������������������������\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�����������\�\0\0\0?\0�MKP�L�{���HWq�H�j�\�vl�ۛ\�@\��2�a�֓��\�\0G��Ϧ\�H\�{H*��\�\0���K\�\�i{Ȣ�#�{�`�\�t\�@\�~pʤ�\0}^\'\�o^j�!\�\�\�\�\�8\�v\�1�x�\�^ӯCka�jz�v�\�\�\�\�I�O,d�\�\���\'\�a��H����B2\�$x\�\0\��TIO��9Q[�u~\0�U���X\�5\�\Zp�l�͒ ��\�ݮu\�=�{:\�!�s�����\"-f�\�H\\��\�ܪ\�\��\0�\�\�\�}3\�~�\�a��Ⱥ��\�\�?��9(\�*J�kFZ�\\��I,΄�*�#�j�\\\�Z�nkx���%s�g�\�\�\��Ld\��B�(� (��\0\�����N�0\"\�@3ӕ#�\�\���͆�}\�y�%\�\�I�W\0\��t\�j�O\�E\�v_5�d\�\�`\0u,B�Ԋ�/\n@�\��Y\�F�O\��J�\� �\0>\rgQ\�x�3��8]Y\�$Qa��\�7\0r~��\�\���\��n��DE�Y�m%#\�\�zt^��&���\�\�r�y^�\�~�R\�%[j(\��<v���ǈ\\�D�QEhsQ@U��Hc��\�\Zo($r]��G�y����Z\�\�\�q\Z�9.\�U du�<������Q�:|짫\�?�o��ۙ\�\�]E$��FT}��	\�C�:�W�\�-��T�\�:��\�b�}!\����\�z���Ի�M��0#�˷�Q�\��_a,\Z\���VY�\�H9\��~#�}NȤ�\'�NX���5\�y�B\�_�	9c���8\�\�\�Q���,!Xgw�\�N\rxC�Kx^y���&Wr\�l�8�r;}+�ӵ8��(\�\�\�X,E�de8\�n\�T󎦦3qe֢�\�\�o�$\Z��\�1�a8u dH>���z\���e9�x����\�\�]\�p�Z\�&d8�A\�\�ϸ5\�\�E\�\\iД9ڡO>խ)6\�\�J\�\�Rh�EV\�9OT�[\r:k�pU~_��J�k)wWm�y�\�*\�ʑ\�z�\�H�{\�j*�M�o�A��3\�_θ\rgL�>k\�!�-ۖ8-\�;<\�7��ֹ\�ܧn\�u(�t�\�\� �\�K$)�yPݐ.}O\�\��¬\�-��oq�P\�\����>�Yr\'�\�޹�>\��\�!wAG*�2�	PG#�\�^=�h�\�Db\�f�\�>\�}�$�y\�=��f�]؆[3q�\�\0w^	X�Dez�\�\'\�\�S��5\�B\�\rE4��ao�2�*]��\0��\��MX�%�\�v$��ogbB\���\�\�\�ߟ^\�l�\��\�k8�\�\�\ZƤ��f�8s7q\�*�\�[�\�x�έw�\�:\�6G\�ebY8���\\��\0�\�|8�xE��g\�\��^\��B\�4KwNk+\�\�He#�F=��\�^iýo@\�\�Lh�-]��/���\0��\�ҪT\�u���\Z�\�\���\�T~\�u�\0?M�\0|\�]8\�y�\0�\�!}�_�kJ��H\���E�\0�\�Ec�Ge_\�S<��i?�\�?�7��/�G�\�1Eٛ\���ۯ�\�W�qo\�\�)�Ҋ+J[����\���QE�\�QE\0�\�','1',NULL,NULL),('16a37d2f-f5fc-4b5f-b547-3207c880940d','Laila Laura','laila@laisa.com','1','9gTIeBtZwRNCUps8hytmqA==','d0c9fc39-b4b1-11e5-964f-829e93055fcd','e91bc78a-b4ae-11e5-964f-829e93055fcd','�\��\�\0JFIF\0\0`\0`\0\0�\�\0C\0		\n\r\Z\Z $.\' \",#(7),01444\'9=82<.342�\�\0C			\r\r2!!22222222222222222222222222222222222222222222222222��\0\0A\0A\"\0�\�\0\0\0\0\0\0\0\0\0\0\0	\n�\�\0�\0\0\0}\0!1AQa\"q2���#B��R\��$3br�	\n\Z%&\'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz�����������������������������������\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\������������\�\0\0\0\0\0\0\0\0	\n�\�\0�\0\0w\0!1AQaq\"2�B����	#3R�br\�\n$4\�%�\Z&\'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz������������������������������������\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�����������\�\0\0\0?\0�Ri3M\�4��\�,~kϼe�!�7z\�u��s\�+)%\�\�Fy\�s�{��\���\�º|s̞m\�\��wm\�q\�\'��瞣ּ\'Q�P�ƻut#�$����\00aǭeR����0@\�>3A>��궉on\�\�Kyi�\��\�\�w�L�\�m5+D���9�q�t`A��5/\�\�v�/\�\�р\�u?��\��s\��4-j;i]��p�dQ�<ק\�J�xȪ\�ICd}^�\�T!\�\0�\�A�\�]68.O\�QPn��\�\�SK\Z\�7\�֘o�\��:�<S\�F�s�x\�\�\�ٙ����\��q뻌�\0�=+���Ӽ�%�\�Zr�@���9�o\'�\0��\��ƺ�\�ky��[�׹O�_�\0e\"�?Z\�4\�oW��k�$Q�\03\�v\����V�>�	Fк\�%\�Y]\�\�\� dr��\n��m|Gw9�+�w�L�`�2�����\�\�*�Lѓ��*G\�^N�,�;�$�r\�r{�\0�\Z���{$ϧt\rJKE��\'V�\�0Glv�{֏�3ָ�^#honS�\�@W����A]\0�V?*��+߂�S>V�\�f\�j\��Vf��\0�ߕ�L��\�j\'���#R7��!w�*�\�\'�\��������c-�\����\r�&\�7C�Ǹ\�8�?�Tg��JKs\��i�7���0Y�\0\�m�)\"\�g��+ե���M\�PI>f�IO˟\�Bq���I8�p3\�4Yt��\�+�T�xNB\�\�1�5\�\�\Z�\�豛�˸�f]�+�\�8\�y5Ҍ�\��XI)F\�=���X4˩_��\�\�\�yЏQ��ˣ.Ar�e���\09\�[au-r�I⶚\��1\�`�1�������w�t)[Q��\�\�\�\���[d{���m\��.؎%W\�d9$\�}ޘ\�s\�8\�k\�\�\�(�_\��-|Gnm\�,\�$o�\0/=��x\�\�j�>\�n&�O�\0y\��{\�g�׹��\�\�\��\�޽��\�����[�\�Usy���\rh�\�W�A\�7\0\�8>�}�\��V�&�\�\�\�3!Q�Wq`q��\r�\�ȩ\�f�I]�a\�o\�oF�c�\�\�cRܕw︀2NO͂kJM\r��o�\�&\�Y���X��?�Z��:��\\^ĒG\�yQ��\�\�\�\�A铷�����e\�\�\�1s\�eaWb\���<\����=���V�}�Q�eA����\0#n\�\�\�Oӡ����(#�*\"�E\�+\�>$�*\�S�mB\�(���F���C�� �\�\�q\�^��Kv\�\�\�(>�\��+7R\�-\�?�+�\�\�!X�\�\0p\�\��]��\�N\���\0%����\�}\�1dӬk�l�M��\�.H=0��\�z$\�L�2��Tu�\���\�i�\�G;�#�����.\�m-����!frF��\'?1�\0h�`��5�am�h Y�\�!cܳ?�O�\�\�J�)s?\n)�b�\0�E2�~���\�\Z\����\�E2�Y�?������\�G�\�=~��+�\r\�\�\�\�#\�\��\��\0e�\���Q]h\�{�t\��\�O��j\�\�E1-\�QE�?�\�','1',NULL,NULL),('1d0a3604-7851-4a1f-bc2e-03c49c305862','Lula Larápio','lula@lula.org','1','uYfm66xbc0VRetSqV/qGlg==','d085d09e-b4b1-11e5-964f-829e93055fcd','e91bc78a-b4ae-11e5-964f-829e93055fcd','�\��\�\0JFIF\0\0`\0`\0\0�\�\0C\0		\n\r\Z\Z $.\' \",#(7),01444\'9=82<.342�\�\0C			\r\r2!!22222222222222222222222222222222222222222222222222��\0\0A\0A\"\0�\�\0\0\0\0\0\0\0\0\0\0\0	\n�\�\0�\0\0\0}\0!1AQa\"q2���#B��R\��$3br�	\n\Z%&\'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz�����������������������������������\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\������������\�\0\0\0\0\0\0\0\0	\n�\�\0�\0\0w\0!1AQaq\"2�B����	#3R�br\�\n$4\�%�\Z&\'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz������������������������������������\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�����������\�\0\0\0?\0�u) ~�ֹ\n�\"�e\�M]���R$��+\���FXx\�[���}�\�\�\�\�v�ֶl�+eŬ9\�?�\�Ss��\�;��?\��\n��t�&Ua4jO\�0>�5\�*���:�I[\�g�\�ɣ_�/�\�IG@\�\n�>�R]x\�\0��*($dB\�H?Bz~?�rZ~�\n��m\� \�\Z\��\�Y\�,���W\�r�\�S��\�\�\�ɘ\��6\��\�\Z#�\��\�c\�=�wƚb\�]\�\�Ѫ��\�\�`\�\�G\�*�s\�l��#��\�̹S�̖�2/�\��\0yh�_\�_\�*o!\��v�I.ą�ג������\�$\�G�z��4�]\\G�G\�\�A\\���[Xޫ�7��U\��?Ҳ\�ל\�\�m��ڑ����Э\�\�	.�N?\��+��O\�\�\�\�Ѵ��\0\�#\"m����<t\�j\�V8֐\�37�	!z\��+��{���\�a�\��KJ5\�(\�RC\�\�v\�\��J\�T��\�\�r1;�\�\'?B*(\�8\�[zv��{\�̝�H�/)��i�ẉ�;��J#$�\�{X\�\�uw�@\��@%�݀*q\�S\�\\Ϋ}q�[\\ۼrc 2\�#5?������d&(�\�ǹ\'�T�@^i�A\"���ܟ\�\�?�1�\�V��E�Z�\\i�\�\�\����v\�\��Q^��\���\�\���\�$�G`���Ig���E`$\�\��\�]kG���Lh	�٬$QN�\'Y.c_ަ\�HS�[�Z�\\=I��D7\�\rT\�\�̛H$d1\�n����s���Uw�4޶�\�A)��\�ް�-q���/��*$*v�\�OoOz�\0��\�\'�Y\"\�`3\�1�+cHӧ��\�m\"��\�F	>�(�\'\�\�\�G�FE�:�\n�ɫ^R\� ֍��5\��#��p?�z�Ӧ�V<�\�*i\'s\�\��\��Q]o�5\����\0ǖ�����\Z\�Q�����E�i#��Ľ\\��Z�\�c������\�\�G7k�n����Lp3\�w> ӵ�61�^G\r�H&�\�\��G^=k�\�\�-\�QO\�/6\�]�dA\0an�\�\�y9\�5��:)ĳ%���r\�t�S�A�[�>-\�ז\������\�\\U8\\cj\�\�\��\rN\�\�Y��X-\�ՑR\�%$T\�7^�WM\�uMGF�6\���Y6\\F\�L\08��@�\��Ts\Zr�O�t�4�2KK\�BmI�C�`�UU2�^�\�j	�)�\�Z�\�\��\0\��)o\�\�h���\�\0\�ўsֱ�KT�ܽާemu`^p���R�*��\�Cԑ��q]�io?�,��}�il4ݓؖ\�\�;�R\�{\�zQ~�q0m�%�\�G0I�!a��0zV���\�.\�R���k;H\�u݄\�\�\�A\r���]��y.ǃ�\0�p1\�5rX^\�i ��x �q�\��EP\�\�tޥ\��H\��?���v\���EO*\�.fd���C��\0��?�Y�;���\��\�%���\ZCs\�g�\0Yg�\0]\��\Z�\��]\��v?�QE%��a{\�>\'�\0�v\��\�v�����R\��L?C��m\'��E��\��	EWa\��\�','1',NULL,NULL),('2f83f593-1a41-46ce-aaf7-96773f01701f','Robô Vereador','robo@robo.org','1','QvtwEotejfVm9BIYIz+Ckw==','d095bee1-b4b1-11e5-964f-829e93055fcd','e91bc78a-b4ae-11e5-964f-829e93055fcd','�\��\�\0JFIF\0\0`\0`\0\0�\�\0C\0		\n\r\Z\Z $.\' \",#(7),01444\'9=82<.342�\�\0C			\r\r2!!22222222222222222222222222222222222222222222222222��\0\0A\0A\"\0�\�\0\0\0\0\0\0\0\0\0\0\0	\n�\�\0�\0\0\0}\0!1AQa\"q2���#B��R\��$3br�	\n\Z%&\'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz�����������������������������������\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\������������\�\0\0\0\0\0\0\0\0	\n�\�\0�\0\0w\0!1AQaq\"2�B����	#3R�br\�\n$4\�%�\Z&\'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz������������������������������������\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�����������\�\0\0\0?\0��(���ѝ�\04\�\�VV�]N\�b��z_:j?�Cq�4\�)k��)l\��u\�v��t�v���\�YK\0Iܩ�#޸[$\�\���\0�zjH\�#\\��>�\r�κhJ�.\��\�\�fw�.�u*\��c�wp�����r���ֵr:^��\�^�j\� \�mc]�w\�`\�c\0=s\�[�ݘ\�k�\�\�-�Pb\�ޅ\'���[O��Tӭ�\�_}�\�K,m\�dU����(��jD�#\�g隷U���	�te+��Mn\'�\�=\�o4\�\�set\�\���\"�+���y�\0�$\Zԑ+b�\�\�Fs]\�䗺���ڭ�\�3$\�\�la��\��3\�+\'C�\��O�\�a���������s\�<թ�G����b:�C�[\'�\�\��I�{�+I�8���\�Rm>\�Q�f�hnĥ\���8�5	X\��W\"YnQh��2��\�7�\���\�J\�^\n\�fм#a�\�ۚ\�<j\�\�S{m?�\�+~��;,2\���2@�\�\�q˘�\�\��J�Ԭ��(����ζ�3��\rKA4\�\��I��X\�8bzgi\��\�?`�H\�\�R��/<\�$\ZSB\�ql\r�\�\�Cc8\�\��S\���\��D�\�<��\\�\��u\�\�>����7�E�;���{f�\0�{�q#9\�V�\�ɫ�m_\��\�Kaii}z\�p[D7�\��z�N3]U��=\�9\�\"�v\�\��\�Ưg���%\�\�]���*m-\�IC��@\r{=�Ry�T/9#�\�&�\�Z\�^\�΍�Ad\�Ի���\�\�\�ƴ\�\�1EAaEPEP7\�\�\���\�59$XZd�Dk���8\�	�+;H�N�\�]\�Z]�\\*�~\�\�O\"���&-^5�\�\�+�rheP\�\�r�|+\�D�\�\�MK	�2АF¶?®.\�I]�־ \���\0���a��\�\��Bo\�\�9�\�C��\�v�ּ�������\�3��\'5\��ѡ\�tht�o3Ȅm�\�r팓\�?Z$5袊�(��(��(��z\��Q@Q@Q@�\�','1',NULL,NULL),('8812e259-9727-46ea-8781-dd0fbfcbe8a0','Administrador','admin@admin','1','Ps1sPbw8+CJQ4B7nhSx6tw==',NULL,'e90e81bb-b4ae-11e5-964f-829e93055fcd','GIF89aA\0A\0�\0\0\0\0\0\0\03\0\0f\0\0�\0\0\�\0\0�\0+\0\0+3\0+f\0+�\0+\�\0+�\0U\0\0U3\0Uf\0U�\0U\�\0U�\0�\0\0�3\0�f\0��\0�\�\0��\0�\0\0�3\0�f\0��\0�\�\0��\0\�\0\0\�3\0\�f\0ՙ\0\�\�\0\��\0�\0\0�3\0�f\0��\0�\�\0��3\0\03\033\0f3\0�3\0\�3\0�3+\03+33+f3+�3+\�3+�3U\03U33Uf3U�3U\�3U�3�\03�33�f3��3�\�3��3�\03�33�f3��3�\�3��3\�\03\�33\�f3ՙ3\�\�3\��3�\03�33�f3��3�\�3��f\0\0f\03f\0ff\0�f\0\�f\0�f+\0f+3f+ff+�f+\�f+�fU\0fU3fUffU�fU\�fU�f�\0f�3f�ff��f�\�f��f�\0f�3f�ff��f�\�f��f\�\0f\�3f\�ffՙf\�\�f\��f�\0f�3f�ff��f�\�f���\0\0�\03�\0f�\0��\0̙\0��+\0�+3�+f�+��+̙+��U\0�U3�Uf�U��U̙U���\0��3��f�����̙����\0��3��f�����̙���\�\0�\�3�\�f�ՙ�\�̙\����\0��3��f�����̙��\�\0\0\�\03\�\0f\�\0�\�\0\�\�\0�\�+\0\�+3\�+f\�+�\�+\�\�+�\�U\0\�U3\�Uf\�U�\�U\�\�U�̀\0̀3̀f̀�̀\�̀�̪\0̪3̪f̪�̪\�̪�\�\�\0\�\�3\�\�f\�ՙ\�\�\�\�\��\��\0\��3\��f\���\��\�\����\0\0�\03�\0f�\0��\0\��\0��+\0�+3�+f�+��+\��+��U\0�U3�Uf�U��U\��U���\0��3��f�����\������\0��3��f�����\�����\�\0�\�3�\�f�ՙ�\�\��\����\0��3��f�����\����\0\0\0\0\0\0\0\0\0\0\0\0!�\0\0�\0,\0\0\0\0A\0A\0\0j\0�	H����*\\Ȱ�Ç#J�H��ŋ3j\�ȱ�Ǐ C�I��ɓ(S�\\ɲ�˗0cʜI��͛8s\�\�ɳ�ϟ@�\nJ��ѣH�*]ʴ�ӧP�J�J��իX�j\�z2 \0;','',NULL,NULL),('be111101-27c3-40ca-9a1f-1dcaf26aad32','He-man da Silva','heman@heman.com','1','eBTsPZjRK6sL2WV6phWBbA==','d1856d73-b4b1-11e5-964f-829e93055fcd','e91bc78a-b4ae-11e5-964f-829e93055fcd','�\��\�\0JFIF\0\0`\0`\0\0�\�\0C\0		\n\r\Z\Z $.\' \",#(7),01444\'9=82<.342�\�\0C			\r\r2!!22222222222222222222222222222222222222222222222222��\0\0A\0A\"\0�\�\0\0\0\0\0\0\0\0\0\0\0	\n�\�\0�\0\0\0}\0!1AQa\"q2���#B��R\��$3br�	\n\Z%&\'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz�����������������������������������\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\������������\�\0\0\0\0\0\0\0\0	\n�\�\0�\0\0w\0!1AQaq\"2�B����	#3R�br\�\n$4\�%�\Z&\'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz������������������������������������\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�����������\�\0\0\0?\0�Ռ�;�\�?;W����Uo\���\�\�PX�Q\�kf\�Z5=b+UD�pv\�\0R{��n��s�j7@�]��`P�\�\��Vsܻ��8�s���\�z(\�\�3�ޠ��36\�r\�3\�L~\��{�c����8�}Z�j��Q�8\\\r8A:��f{xCM���`Q��\\�$�\��\�߆\�\�\�il�\�<\�\�kbİ8 g��R3�Mk��\�K��\�=ۏ�RC~X4\�>l\��C�<o\��\�\�+6:�*�Sw]M�8XU�\�\�l\���N�����c��#\�\�z\��\�y�����ķ�\�g2ZA�\�]��˞x\�\�zWg\�Ʌ�\0\�g��7(�LʂA\�J+ۛA(��\��#�<2��\�\�}|o(\�\'\�(%�s\�/\�E[\��Ers\�\">�6\�m���&��\� pW�\�X^%��m�OY?p\�(0O�9bG�Hs�\�x.��\�\�\�Q\�q0(��l\�\�\0팓��\��Av��__ɸIk\nڌ�\�\�\�\�^:���Uϋ�8ѓK�\�\�ah�V�Ջ��~z��\�7�Ɂ�\0�3\�p�\r�Շڦw�\��ܨyH\�\��z�yϨ�	\'i5L�\r���!\'\��MMp�Ͷ\'�a\'8r0Fx�\��+\rZv}Or���c?R���xm\�m��RUB\�=N\�N\�\�B��\�kR\�.��&�ɕb~g��\'�b�\�a\�`H\�\�uUX2�y�#�\�\�P�\�\Z\�`��u�$�S�:�7lL4�5����T\�tU\����t\�K#˷�9c���p3�(�gm�\\\�|�㍁�X�\�\0O\�x�=\�gw?\�<\�®~Uh՛�l�\�޹-.V��Y�\�E�\�L���<�\0J��x�����\�\�(�Uө\��\0gI�\0?\�W��o�\�k�\0\�7��\0?\�EW��s�\�\�;~dZ�-��k$?\"ț�\�FA\�\0~u\�\�Ee�\�\�Ķ��ÒJ��00NFW�\�s�\�kʮ-\�\�tx\���\� �$t�r,z�ں\�\�=��l^�\��d�%�2\����=�]7��%V-Ŝ\�\'[���{�\0�-�\�y/\�\�x�-[ɑ$NW\�\��,1��\�\"�K<.\�`9�)6�}����.�\0(\�^*���c\�\�ڕ!����e�\��X\�9�	?\Z\�\�5Y4����\�\�\�nT�p9\'=�\� tn�㠨��9\�H\�E�7\0a�ޝ���\�w��\�˙�\��cf��\�s��\�>��4��\\�0Q\�\�\�}�}k�\�t�o�Z\�)^$\�(T\�t\�5��xJ\��\�uÍ��n\�s��0@\�Y\�T`�\�\�y��\�Դ:w.m\��\0<���(�O�����ϛ\���g�ȞM6\�0-�p)2�\��q�)�\�B\�\�0ME.2b��\�؏q�]5\��\�rJ�\��\�)��\�\�\�\\ �z�\�PGC\�)\�\�\�\�I\�\Z\�}9|\Z3&\��\�\�[|c��\���U�ku%\��x�>�<Gr���.�&\�\�\�c-� 	\�Bx�F{�\�Ե��e��H��\�>�\�S	GiCFsCW\r/g[Tt\�ʨK�1�����\�\�\��J\��b{qߚ\�\�\\\�<׍䓧\�b�G��֫�\�\�\�$ޤ�X���?J�)�\�Z�\����\�9\�;\��f����\�JD�{\�\n\�D��8\���\��sL�y�8y��\�\�\��\�]S\0\�U�*F<�]���p�\�qe�v��\�=��_\��\��\���?�lzW,I�)h������\0 K\����\�@\�E\�\��G���Q3u?����\0u�\0�ZK���\0�M�֊*j\��\�[/\��3�B\����\0\�k]�\�(������\�k��\�:�(�3��\�','1',NULL,NULL),('c20d2870-30f7-42ec-acfb-d1e854758f1c','Alê Caricato da Silva','ale@ale.com','1','RwPkz/TGy09tNQN1TriYHQ==','d114b7b4-b4b1-11e5-964f-829e93055fcd','e91bc78a-b4ae-11e5-964f-829e93055fcd','�\��\�\0JFIF\0\0`\0`\0\0�\�\0C\0		\n\r\Z\Z $.\' \",#(7),01444\'9=82<.342�\�\0C			\r\r2!!22222222222222222222222222222222222222222222222222��\0\0A\0A\"\0�\�\0\0\0\0\0\0\0\0\0\0\0	\n�\�\0�\0\0\0}\0!1AQa\"q2���#B��R\��$3br�	\n\Z%&\'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz�����������������������������������\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\������������\�\0\0\0\0\0\0\0\0	\n�\�\0�\0\0w\0!1AQaq\"2�B����	#3R�br\�\n$4\�%�\Z&\'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz������������������������������������\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�����������\�\0\0\0?\0�=g��ZٵƟq<�Z7\�\��\�\�#k\�!�����)\��\�*\�&\�\Z��<�m\�]��?IO{mnȓ\\E?\�\�g\��\�Ɠ^Kw��fY�k�H�3�>f\0<���8\�jbjΟ$\�6�]���uih\�k���\�3�\�ڙb\�\�[\�:��t��onn�\�\��9S\�=�zWa\�\�\0\�^\"K+��1iNw\�J�@��&<\����O��S�˩5ͬ\�k���E��<\�\�\���1�R�\�\�^4e%t|\����\��\0\�F�\\L�\��\0��z���OQX.O���H�h���p\�J\�\�)�� �3�|��\�\�L��\��\�K{�\�\Z9UFq�\�\��\�\��7��\�5\"�0F]�auAa�J�_@Fr0q_S蚗����\�&/)����\�NvPJ�\�\�_\�M}��k�c7���\�3�)�l�?J��#�\�4\�q)�kS�B3;m+�\�?�tQ�*��\�^2�FU6nǳQ\\��\'\�o��\�\�+�\0\�QU�J�\0\�\��\0�0���\�p\�\�&y[\�\�I�\�\�u+D��\�\�\�\�\rƢ#�ѹ\�$��3\�B�\�k\��\�D\�4qK,QL\�\�+:���\�V\��1�E�+H��\�:0�r���S\�C\"F���F\'o]�g<�\�\�jF<�\�\�y\\5���뷾�\�֫y\�GҼ=g%���U[\�C\�\�o�̻X\��\�\�\�|ui�\�pXA�jQI$�<Ѐ��\�ޤ�\��!V�\�7\�Vq\��U]J6V�K[\�ܟ0ټ+vq�3ۜ�z6��h\�d\"�;�ȸ�(>Ύ\�\�\n27\��\0<	(\�\�\��\��)k\�\"�\�\�\�84Y�\"\�U�fH\�6[*\�\�`\0\�\�\�\�1\��\�j>#�8�%�YRbӼjq�2q�� ~�\�\�ogr;�u��\�\���\�Ў\�\�\�9�\��+M\�uq��㻵I7A�\�+���1f�4`}\�\�\��T\��\�_c\�|i\�\�x\�M�\�a\�qo��������H\�ba��\�D��ЬQ�\�!pz�#-�\��NѴ˯\�ww����U&���FC�	\�˸�\�p{A!�ѵ���*�1\�!M��Y�\�j�>U�\�(\�\�\�$ױ�W�jr˩�G���\��\�/cC�}(��>\�5a-��\\čm(1ȣ9*x89\��\0\� ���\��\�\�\\�\�y^\�\�y\�2����\�]��p\0U�<0�\�\�<���\�\�+���Ri\�A�\��;\�\�\�~V\�\�2�A\'\�\Z�y��j�\�_\��X(O+\�V��\��\��tiw\�\�P�GF��\Zŭ\�̖R�s���k@�\�\�H\��T��\�F3U�vY\�M7Te�r\�a�\0*ݪ���\0\�q��Mw\�V\����\�YL+	�v\�r\�@H\0�\�MDZz�}d**�扭��\�|n�J�0R\�Fv���\�\�\��\�\�*N�f��H?)v\��9\�\�<M�b�8�֣0&u裡�C�(	\�\�\'�8,9MF<\�c��\�\�[\�j\�8�7���wQ \�h\�X�\�=��\���u$g̒�i\�\��\0\02y\�*��N2y5\�\���9mq�<2\�v7���\�揧�\�\�鑃\\��;O�\�1ޮc\\��9ϸ��\�ʬ!g$\�\�f����Yo���\�?�S\�\�_�\��Ɗ��p�\0\���\�?��25�\'��{[\�\'�k�KK��XZi\�{\�vTE\�\�NN@q\�Y��\'k)�^i�,�y�+7Q ���c\�\���I�\�c밴y�ZH�s=��\�\�׮��Z#$��e-�:�U\0��\�\�Q^�q�x�K�΅}6�}�i�h\�9!ܛ�\�0Hx\�Y�N��ȯ1DuӖ\\�\�\�pB\�\0ϱ\�U\�\�o5/���i/mI���Gu�\�\08\�͜��\0\�Q�]\�m�\�d�.���\�W\Z��喧��#5\��\��*�LJ%�\�\r�\�\��rq^w�^\�ǩ\�Wp\�3\�\r\�\�\�p�2F�d\�v��&\0˜`��}@��\0�#J֎��[��ώ=�@J,�N;�\�\�\�\��\�y-�Ht\�\"pL�\�q�.�8�\�\��4\�EE ��K�l{�\�\r\�\�ދo~\�\ZJ�]��y*>l6@\�\0�5o\��E\�\�d���\�6\�.ȦA\�6}ܰ\0Ü�Es��\�\�Ě��\�^	$[j�C�w��@\��t\�鍦Ŧ\�ǥ܉m�;I �3m;{�b{~��]\�\�	\�\�W\�<?�\0C�\�\�\�c��\0��tQ\\W��W��\��\��_\�_T�\0��蘨��ܫ����N�?\�e\��-\��\0\�M��^\���I0�\0��_�1袾��\�\�+�ǟ�H������\0D��i\�\r�׫\�B�*�\�\�\�Λ�_\����M]r\�\��\0�\�\'��z(�\�ж/QE\��\�','1',NULL,NULL),('e0b38f7a-e452-4563-9737-d742e580cc7b','Alessandro Paulino','alepaulino@ig.com.br','1','uW4k6Lk0m871CiS3jPjqcw==','d1856d73-b4b1-11e5-964f-829e93055fcd','e91bc78a-b4ae-11e5-964f-829e93055fcd','�\��\�\0JFIF\0\0`\0`\0\0�\�\0C\0		\n\r\Z\Z $.\' \",#(7),01444\'9=82<.342�\�\0C			\r\r2!!22222222222222222222222222222222222222222222222222��\0\0A\0A\"\0�\�\0\0\0\0\0\0\0\0\0\0\0	\n�\�\0�\0\0\0}\0!1AQa\"q2���#B��R\��$3br�	\n\Z%&\'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz�����������������������������������\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\������������\�\0\0\0\0\0\0\0\0	\n�\�\0�\0\0w\0!1AQaq\"2�B����	#3R�br\�\n$4\�%�\Z&\'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz������������������������������������\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�����������\�\0\0\0?\0Ͽ�3\��*�^F\�\\�Õ(�s\��\�6��n%\Z�06�\�oZȴd\�L�F\�9�\�+��\�g�S\�\�\�J��m��[[.9\��Q\�k�\��ð�w�qI\�1\�\��=�;�}\��L͏Zе֘��\'ۊ���k�2a\'ҹ�|4{h\�L���>Sw�T�H�\�\Zi\\�\��2�����\�W2�k!#p?)\�º����L\�3|��U�\0+ڊt�\�X^(\\\�7C\��\0�\n\�BlV>��\�.�\�\�>�����<\reV\�U_�\�&�;M5\��e\�Ҵ�U��2�p3�\�\��F�\n�X\�]\�\�Vq��W,�\�sԦ\�b�\�a��\�\�Y7\�\�J�m�O.�n��\n��L\�:\�\'\�ndx\�\�BG@�O\�S���w9[L~6�TM�9\�Ӿ5�\��O�2^\�\�\�N��\'�>S�x\�\�S���ky�զ\�R\�\�EZ�誱�z�~֩ݠtu=V\��\�Q�j�\�~)�N̩�Z%��-\�8p�A�V��ᕉ�B2\�/,e\��E\�U�����}:��Ę\�1\�k�\�Jǭ�,�~�0E,��7~\Z�m�\"��*2\�?S\\t:\�\�\�\�-��bi>^���ۥw:\'�mu28\��2�΍\�\ZZEs\�Z7�,W�� �b��\�L1ի�d\�3�폟Δ\�\�]4��y���\�S�\�^Y���09h�Uj�\�^�pO�R��\�\�8&O��\0\Z\�b�\�ksH�\���U�F8��\�`~�Y���-Ya\�\ZX�\��\�0��+X5�&�Ff*��4��٤5�K�\\��?\��\\W;gic*\�k+2�\�F\���+3��gn\�\�K[ϰ�.�nW#>\�wv�\�-<\�z7\�(a�g\�9��KW���\�\�ud\�\�M^]&�Re�\�\�| ܰ�g\'��7u\�-\r]\Z\����Z�\�xV0?\�kY�nc\�r}A�dd�+\0#(\�t\�Ȯ�E#�0Etr\�|\�ܛg7\�\�ϻ�c�h�\��P+�3��\0\�J\���\n��?\��V�\�!��\0���6o����\0���+<V\�\��&�\�V��\�+����:/��\�U�w�\0\�(��\��3Ω�\n(���\�','1',NULL,NULL);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `votos`
--

DROP TABLE IF EXISTS `votos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `votos` (
  `IDSessao` char(36) COLLATE latin1_general_ci NOT NULL,
  `IDUsuario` char(36) COLLATE latin1_general_ci NOT NULL,
  `voto` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`IDSessao`,`IDUsuario`),
  KEY `usuarios_votos_idx` (`IDUsuario`),
  CONSTRAINT `sessoes_votacao_votos` FOREIGN KEY (`IDSessao`) REFERENCES `sessoes_votacao` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `usuarios_votos` FOREIGN KEY (`IDUsuario`) REFERENCES `usuarios` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `votos`
--

LOCK TABLES `votos` WRITE;
/*!40000 ALTER TABLE `votos` DISABLE KEYS */;
/*!40000 ALTER TABLE `votos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-08 13:19:04
