﻿var relogio = {
    inicializar: function () {
        relogio.registrarAcoes();
        setTimeout(function () {
            relogio.atualizarRelogios();
        }, 1000);
    },
    registrarAcoes: function () {
        $(document).on('click', '.playAparte', relogio.aoAcionarPlayPause);
        $(document).on('click', '.playPalavra', relogio.aoAcionarPlayPause);
        $(document).on('click', '.pauseAparte', relogio.aoAcionarPlayPause);
        $(document).on('click', '.pausePalavra', relogio.aoAcionarPlayPause);
        $(document).on('click', '.stop', relogio.aoAcionarStop);
    },
    aoAcionarPlayPause: function () {
        var url = "/PainelVotacao/IniciarPausarRelogio/";
        var tipo = $(this).attr('data-tipo');
        var botao = $(this).attr('data-botao');
        var intervalo = 0;
        if (tipo == "APARTE")
            intervalo = $("#IntervaloAparte").val();
        else if (tipo == "PALAVRA")
            intervalo = $("#IntervaloPalavra").val();
        if (intervalo == "")
        {
            alert("Informe o intervalo em minutos");
            return false;
        }
        else if(parseInt(intervalo)<=0)
        {
            alert("O intervalo em minutos deve ser maior que zero");
            return false;
        }
        var data = { idCamara: $("#IDCamaraMunicipal").val(), tipo: tipo, intervalo: intervalo,botao: botao };
        $.ajax({
            url: url,
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            success: function(r){
                //var classeBotao = (tipo=="APARTE")?"playAparte":((tipo=="PALAVRA")?"playPalavra":"");
                //if(r=="PAUSADO")
                //{
                //    relogio.setarIcone(classeBotao, "pause", "play");
                //}
                //else if (r == "EXECUTANDO") {
                //    relogio.setarIcone(classeBotao, "play", "pause");
                //}
            }
        });
    },
    aoAcionarStop: function () {
        var url = "/PainelVotacao/PararRelogio/";
        var tipo = $(this).attr('data-tipo');
        var intervalo = 0;
        if (tipo == "APARTE")
            intervalo = $("#IntervaloAparte").val();
        else if (tipo == "PALAVRA")
            intervalo = $("#IntervaloPalavra").val();
        var data = { idCamara: $("#IDCamaraMunicipal").val(), tipo: tipo };
        $.ajax({
            url: url,
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            //success: function (r) {
            //    var classeBotao = (tipo == "APARTE") ? "playAparte" : ((tipo == "PALAVRA") ? "playPalavra" : "");
            //    if (r == "PARADO") {
            //        relogio.setarIcone(classeBotao, "pause", "play");
            //    }
            //}
        });
    },
    obterRelogioPalavra: function () {
        if ($("#IDCamaraMunicipal") != null && $("#IDCamaraMunicipal").val() != undefined) {
            var url = "/PainelVotacao/ObterRelogio/";
            var data = { idCamara: $("#IDCamaraMunicipal").val(), tipo: "PALAVRA" };
            $.ajax({
                url: url,
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json',
                success: relogio.aposObterRelogioPalavra
            });
        }

    },
    obterRelogioAparte: function () {
        if ($("#IDCamaraMunicipal") != null && $("#IDCamaraMunicipal").val() != undefined) {


            var url = "/PainelVotacao/ObterRelogio/";
            var data = { idCamara: $("#IDCamaraMunicipal").val(), tipo: "APARTE" };
            $.ajax({
                url: url,
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json',
                success: relogio.aposObterRelogioAparte
            });
        }

    },
    aposObterRelogioPalavra: function (r) {
        if (r == "00:00:00")
            relogio.setarIcone("playPalavra", "pause", "play");
        var rel = r.split(':');
        //if (rel[0] == "00" && rel[1] == "00" && parseInt(rel[2]) <= 30 && parseInt(rel[2]) > 0) {
        //    if ($("#relogio_palavra").html() == "") {
        //        $("#relogio_palavra").html(r);
        //    }
        //    else {
        //        $("#relogio_palavra").html("");
        //    }
        //}
        //else {
        $("#relogio_palavra").html(r);
        relogio.obterRelogioPalavra();

        //}
    },
    setarIcone:function(classe,iconeDe,iconePara)
    {
        if ($("."+classe).children(".glyphicon-"+iconeDe).length > 0)
            $("."+classe).children(".glyphicon-"+iconeDe).removeClass("glyphicon-"+iconeDe).addClass("glyphicon-"+iconePara);
    },
    aposObterRelogioAparte: function (r) {
        if (r == "00:00:00")
            relogio.setarIcone("playAparte", "pause", "play");
        var rel = r.split(':');
        //if (rel[0] == "00" && rel[1] == "00" && parseInt(rel[2]) <= 30 && parseInt(rel[2]) > 0) {
        //    if ($("#relogio_aparte").html() == "") {
        //        $("#relogio_aparte").html(r);
        //    }
        //    else {
        //        $("#relogio_aparte").html("");
        //    }
        //}
        //else {
        $("#relogio_aparte").html(r);
        relogio.obterRelogioAparte();

        //}
    },
    atualizarRelogios: function () {
        setTimeout(function () {
            relogio.obterRelogioPalavra();
            relogio.obterRelogioAparte();
        }, 1000);
    }
};
$(document).ready(relogio.inicializar);