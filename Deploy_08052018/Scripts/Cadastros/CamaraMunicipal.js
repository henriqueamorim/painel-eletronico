﻿var camaraMunicipal = {
    inicializar: function () {
        camaraMunicipal.registrarAcoes();
    },
    registrarAcoes: function () {
        $(document).on('click', '.inserir', camaraMunicipal.aoInserir);
        $(document).on('click', '.salvar', camaraMunicipal.aoSalvar);
        $(document).on('click', '.excluir', camaraMunicipal.aoExcluir);
        $(document).on('click', '.editar', camaraMunicipal.aoEditar);
        $(document).on('click', '.cancelar', camaraMunicipal.aoCancelar);
        $(document).on('click', '.abrirPainel', camaraMunicipal.aoAbrirPainel);
        $('.imagemBrasao').on('change', function (e) {
            var id = e.target.id.split('_')[1];
            var file = e.target.files[0];
            if ($(this).get(0).files.length > 0) { 
                var fileSize = $(this).get(0).files[0].size;
                if (fileSize > 500000)
                {
                    alert("ATENÇÃO !!!\n\nO tamanho do arquivo da imagem não pode ultrapassar 500 kb e a dimenção deve ser de 200 píxels de altura por 200 píxels de largura.");
                    return false;
                }
            }
            var fileReader = new FileReader();

            fileReader.onload = function (e) {
                var base64 = e.target.result;
                $("#img_" + id)[0].src = base64;
                $('input[name="BrasaoBase64String"]', "#form_" + id).val(base64.split(',')[1]);
            };

            fileReader.readAsDataURL(file);
        });
    },
    aoAbrirPainel: function () {
        var id = $(this).attr('data-id');
        var data = $("#form_" + id).serializeObject();
        if (data.ID == "00000000-0000-0000-0000-000000000000")
        {
            alert("Para exibir o painel primeiro é necessário salvar este registro !");
        }
        else
        {
            if (data.Diretorio == null || data.Diretorio == "") {
                alert("Informe o diretório de acesso desta câmra municipal !");
            }
            else
            {
                var w = 500;
                var h = 500;

                var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
                var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

                var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
                var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

                var left = ((width / 2) - (w / 2)) + dualScreenLeft;
                var top = ((height / 2) - (h / 2)) + dualScreenTop;
                var newWindow = window.open("/Painel/" + data.Diretorio, "Painel Eletrônico", 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
            }
        }
    },
    aoInserir: function () {
        $('#Inserir, #Inserir tr').toggle();
        if ($(".inserir").children(".glyphicon-plus").length > 0)
            $(".inserir").children(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
        else
            $(".inserir").children(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
    },
    aoSalvar: function () {
        var url = "/CamaraMunicipal/Salvar/";
        var id = $(this).attr('data-id');
        var data = $("#form_" + id).serializeObject();
        $.ajax({
            url: url,
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            success: camaraMunicipal.aposSalvar
        });
    },
    aposSalvar: function (json) {
        if (json.sucesso) {
            alert("Registro Salvo com sucesso!");
            camaraMunicipal.atualizar();
        } else {
            alert(json.erro);
        }
    },
    aoEditar: function () {
        var trAtual = $(this).parent().parent();
        trAtual.toggle();
        trAtual.next().toggle();
    },
    aoCancelar: function () {
        var trAtual = $(this).parent().parent();
        if (trAtual.parent().attr("id") == "Inserir")
            camaraMunicipal.aoInserir();
        else {
            trAtual.prev().toggle();
            trAtual.toggle();
        }
    },
    aoExcluir: function () {
        if (confirm("Todos os dados relacionados à esta Câmara também serão excluidos (Usuário e Votações)!\n\nDeseja realmente excluir esse registro?")) {
            var id = $(this).attr('data-id');
            var data = $("#form_" + id).serializeObject();
            var url = "/CamaraMunicipal/Excluir/";
            $.post(url, data, camaraMunicipal.aposExcluir);
        }
    },
    aposExcluir: function (json) {
        if (json.sucesso) {
            alert("Registro excluido com sucesso!");
            camaraMunicipal.atualizar();
        } else {
            alert(json.erro);
        }
    },
    atualizar: function () {
        location.reload(true);
    }
};
$(document).ready(camaraMunicipal.inicializar);