﻿var painel = {
    inicializar: function () {
        painel.registrarAcoes();
        setTimeout(function () {
            painel.atualizarVereadores();
        }, 5000);
        setTimeout(function () {
            painel.atualizarHoraAtual();
        }, 1000);
    },
    atualizarVereadores:function()
    {
        var diretorio = location.href.substring(location.href.lastIndexOf('/'), location.href.length);
        var url = "/PainelVotacao/ListaVereadores" + diretorio;
        $("#ListaVereadores").load(url, null, function () {
            setTimeout(function () {
                painel.atualizarVereadores();
                painel.obterTotais();
            }, 5000);
        });
    },
    obterTotais:function(){
        var url = "/PaginaVotacao/ObterTotais/";
        var data = { idSessao : $("#IDSessao").val() };
        $.ajax({
            url: url,
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            success: painel.aposObterTotais
        });

    },
    obterRelogioPalavra: function () {
        var url = "/PainelVotacao/ObterRelogio/";
        var data = { idCamara: $("#IDCamaraMunicipal").val(), tipo: "PALAVRA" };
        $.ajax({
            url: url,
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            success: painel.aposObterRelogioPalavra
        });

    },
    obterRelogioAparte: function () {
        var url = "/PainelVotacao/ObterRelogio/";
        var data = { idCamara: $("#IDCamaraMunicipal").val(), tipo: "APARTE" };
        $.ajax({
            url: url,
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            success: painel.aposObterRelogioAparte
        });

    },
    aposObterTotais: function (json) {
        $("#TotalPresentes").html(json.TotalPresentes);
        $("#TotalAusentes").html(json.TotalAusentes);
        $("#TotalGeral").html(json.TotalGeral);
        $("#TotalSim").html(json.TotalSim);
        $("#TotalNao").html(json.TotalNao);
        $("#TotalAbstencao").html(json.TotalAbstencao);
        if (json.ResultadoVotacao != undefined && (json.ResultadoVotacao == "APROVADO" || json.ResultadoVotacao == "REPROVADO"))
        {
            $("#ResultadoVotacao").html(json.ResultadoVotacao);
            $("#ResultadoVotacaoDiv").show();
        }
        else
        {
            $("#ResultadoVotacao").html("");
            $("#ResultadoVotacaoDiv").hide();
        }
    },
    aposObterRelogioPalavra: function (r) {
        if (r == "00:00:00")
            painel.setarIcone("playPalavra", "pause", "play");
        var rel = r.split(':');
        if (rel[0] == "00" && rel[1] == "00" && parseInt(rel[2]) <= 30 && parseInt(rel[2]) > 0) {
            if ($("#relogio_palavra").html() == "") {
                $("#relogio_palavra").html(r);
            }
            else {
                $("#relogio_palavra").html("");
            }
        }
        else {
            $("#relogio_palavra").html(r);
        }
    },
    setarIcone: function (classe, iconeDe, iconePara) {
        if ($("." + classe).children(".glyphicon-" + iconeDe).length > 0)
            $("." + classe).children(".glyphicon-" + iconeDe).removeClass("glyphicon-" + iconeDe).addClass("glyphicon-" + iconePara);
    },
    aposObterRelogioAparte: function (r) {
        if (r == "00:00:00")
            painel.setarIcone("playAparte", "pause", "play");
        var rel = r.split(':');
        if (rel[0] == "00" && rel[1] == "00" && parseInt(rel[2]) <= 30 && parseInt(rel[2]) > 0) {
            if ($("#relogio_aparte").html() == "") {
                $("#relogio_aparte").html(r);
            }
            else {
                $("#relogio_aparte").html("");
            }
        }
        else {
            $("#relogio_aparte").html(r);
        }
    },
    atualizarHoraAtual: function () {
        var d = new Date();
        var h = d.getHours() < 10 ? "0" + d.getHours() : d.getHours();
        var m = d.getMinutes() < 10 ? "0" + d.getMinutes() : d.getMinutes();
        var s = d.getSeconds() < 10 ? "0" + d.getSeconds() : d.getSeconds();
        var horaMinuto = h + ":" + m + ":" + s;
        $("#horaAtual").html(horaMinuto);
        setTimeout(function () {
            if (document.location.href.toLowerCase().indexOf("painel") >= 0)
            {
                painel.atualizarHoraAtual();
                painel.obterRelogioPalavra();
                painel.obterRelogioAparte();
            }
            else
            {
                relogio.obterRelogioPalavra();
                relogio.obterRelogioAparte();
            }
        }, 1000);
    },
    abrirPainel: function (diretorio) {
        var w = 500;
        var h = 500;

        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
        var top = ((height / 2) - (h / 2)) + dualScreenTop;
        var newWindow = window.open("/Painel/" + diretorio, "Painel Eletrônico", 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
    },
    registrarAcoes: function () {
        document.onwebkitfullscreenchange = painel.fechar;
        document.addEventListener("keydown", function (e) {
            if (e.keyCode == 13) {
                painel.toggleFullScreen();
            }
        }, false);
    },
    fechar: function () {
        if (document.webkitFullscreenElement == null) {
            $("#painel").hide();
            $("#instrucoes").show();
            window.close();
        }
    },
    toggleFullScreen: function () {
        var videoElement = document.getElementById("painel");
        if (!document.mozFullScreen && !document.webkitFullScreen) {
            if (videoElement.mozRequestFullScreen) {
                videoElement.mozRequestFullScreen();
            } else {
                videoElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
                $("#painel").show();
                $("#instrucoes").hide();
            }
        } else {
            if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else {
                document.webkitCancelFullScreen();
            }
        }
    }
};
$(document).ready(painel.inicializar);