﻿var vereador = {
    inicializar: function () {
        vereador.registrarAcoes();
    },
    registrarAcoes: function () {
        $(document).on('click', '.inserir', vereador.aoInserir);
        $(document).on('click', '.salvar', vereador.aoSalvar);
        $(document).on('click', '.excluir', vereador.aoExcluir);
        $(document).on('click', '.editar', vereador.aoEditar);
        $(document).on('click', '.cancelar', vereador.aoCancelar);
        $(document).on('change', '.filtroCamara', vereador.aoFiltrar);
        $('.imagemFoto').on('change', function (e) {
            var id = e.target.id.split('_')[1];
            var file = e.target.files[0];
            if ($(this).get(0).files.length > 0) {
                var fileSize = $(this).get(0).files[0].size;
                if (fileSize > 500000) {
                    alert("ATENÇÃO !!!\n\nO tamanho do arquivo da imagem não pode ultrapassar 500 kb e a dimenção deve ser de 200 píxels de altura por 200 píxels de largura.");
                    return false;
                }
            }
            var fileReader = new FileReader();

            fileReader.onload = function (e) {
                var base64 = e.target.result;
                $("#img_" + id)[0].src = base64;
                $('input[name="FotoBase64String"]', "#form_" + id).val(base64.split(',')[1]);
            };

            fileReader.readAsDataURL(file);
        });
    },
    aoInserir: function () {
        $('#Inserir, #Inserir tr').toggle();
        if ($(".inserir").children(".glyphicon-plus").length > 0)
            $(".inserir").children(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
        else
            $(".inserir").children(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
    },
    aoFiltrar: function () {
        document.location.href = "/Cadastros/Vereador?idCamara="+$(this).val();
    },
    aoSalvar: function () {
        var url = "/Vereador/Salvar/";
        var id = $(this).attr('data-id');
        var data = $("#form_" + id).serializeObject();
        if (data.Ativo.length > 0)
            data.Ativo = data.Ativo[0];
        $.ajax({
            url: url,
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            success: vereador.aposSalvar
        });
    },
    aposSalvar: function (json) {
        if (json.sucesso) {
            alert("Registro Salvo com sucesso!");
            vereador.atualizar();
        } else {
            alert(json.erro);
        }
    },
    aoEditar: function () {
        var trAtual = $(this).parent().parent();
        trAtual.toggle();
        trAtual.next().toggle();
    },
    aoCancelar: function () {
        var trAtual = $(this).parent().parent();
        if (trAtual.parent().attr("id") == "Inserir")
            vereador.aoInserir();
        else {
            trAtual.prev().toggle();
            trAtual.toggle();
        }
    },
    aoExcluir: function () {
        if (confirm("Todos os dados relacionados à este Vereador também serão excluidos !\n\nDeseja realmente excluir esse registro?")) {
            var id = $(this).attr('data-id');
            var data = $("#form_" + id).serializeObject();
            var url = "/Vereador/Excluir/";
            $.post(url, data, vereador.aposExcluir);
        }
    },
    aposExcluir: function (json) {
        if (json.sucesso) {
            alert("Registro excluido com sucesso!");
            vereador.atualizar();
        } else {
            alert(json.erro);
        }
    },
    atualizar: function () {
        location.reload(true);
    }
};
$(document).ready(vereador.inicializar);