﻿var votacao = {
    inicializar: function () {
        votacao.registrarAcoes();
    },
    registrarAcoes: function () {
        $(document).on('click', '.inserir', votacao.aoInserir);
        $(document).on('click', '.salvar', votacao.aoSalvar);
        $(document).on('click', '.excluir', votacao.aoExcluir);
        $(document).on('click', '.editar', votacao.aoEditar);
        $(document).on('click', '.cancelar', votacao.aoCancelar);
        $(document).on('click', '.votar', votacao.aoVotar);
        $(document).on('click', '.seAutenticar', votacao.aoSeAutenticar);
        $(document).on('click', '.selecionarOpcao', votacao.aoSelecionarOpcao);
        $(document).on('click', '.sair', votacao.aoSair);
        $('.date').datepicker({
            language: 'pt-BR'
        });
    },
    aoSelecionarOpcao: function () {
        $(".selecionarOpcao").removeClass("active").addClass("notActive");
        $(".selecionarOpcao").removeClass("btn-primary").addClass("btn-default");
        $(this).removeClass("notActive").addClass("active");
        $(this).removeClass("btn-default").addClass("btn-primary");
        var i = $(this).attr('data-title');
        i = (i==0)?"SIM":((i==1)?"NAO":((i==2)?"ABSTENCAO":"PRESENTE"));
        $("#voto").val(i);
    },
    aoInserir: function () {
        $('#Inserir, #Inserir tr').toggle();
        if ($(".inserir").children(".glyphicon-plus").length > 0)
            $(".inserir").children(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
        else
            $(".inserir").children(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
    },
    aoSalvar: function () {
        var url = "/PaginaVotacao/Salvar/";
        var id = $(this).attr('data-id');
        var data = $("#form_" + id).serializeObject();
        $.ajax({
            url: url,
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            success: votacao.aposSalvar
        });
    },
    aoSair: function () {
        var url = "/PaginaVotacao/Sair/";
        $.ajax({
            url: url,
            type: 'GET',
            contentType: 'application/json',
            success: votacao.aposSair
        });
    },
    aposSair: function (json) {
        if (json.sucesso) {
            votacao.atualizar();
        } else {
            alert(json.erro);
        }
    },
    aoSeAutenticar: function ()
    {
        var url = "/PaginaVotacao/Autenticar/";
        var data = $("#formVotacao").serializeObject();
        if (data.idVereador == "") {
            alert("Selecione seu nome na lista")
            return false;
        }
        if (data.senha == "") {
            alert("Informe sua senha numérica")
            return false;
        }
        $.ajax({
            url: url,
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            success: votacao.aposSeAutenticar
        });
    },
    aoVotar: function () {
        var url = "/PaginaVotacao/Votar/";
        var data = $("#formVotacao").serializeObject();
        if (data.voto == "") {
            alert("Selecione sua opção de voto")
            return false;
        }
        if (data.idVereador == "") {
            alert("Selecione seu nome na lista")
            return false;
        }
        //if (data.senha == "") {
        //    alert("Informe sua senha numérica")
        //    return false;
        //}
        $.ajax({
            url: url,
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            success: votacao.aposVotar
        });
    },
    aposSalvar: function (json) {
        if (json.sucesso) {
            alert("Registro Salvo com sucesso!");
            votacao.atualizar();
        } else {
            alert(json.erro);
        }
    },
    aposSeAutenticar: function (json) {
        if (json.sucesso) {
            votacao.atualizar();
        } else {
            alert(json.erro);
        }
    },
    aposVotar: function (json) {
        if (json.sucesso) {
            alert("Voto registrado com sucesso!");
            votacao.atualizar();
        } else {
            alert(json.erro);
        }
    },
    aoEditar: function () {
        var trAtual = $(this).parent().parent();
        trAtual.toggle();
        trAtual.next().toggle();
    },
    aoCancelar: function () {
        var trAtual = $(this).parent().parent();
        if (trAtual.parent().attr("id") == "Inserir")
            votacao.aoInserir();
        else {
            trAtual.prev().toggle();
            trAtual.toggle();
        }
    },
    aoExcluir: function () {
        if (confirm("Todos os votos registrados desta votação também serão excluidos !\n\nDeseja realmente excluir esse registro?")) {
            var id = $(this).attr('data-id');
            var data = $("#form_" + id).serializeObject();
            var url = "/PaginaVotacao/Excluir/";
            $.post(url, data, votacao.aposExcluir);
        }
    },
    aposExcluir: function (json) {
        if (json.sucesso) {
            alert("Registro excluido com sucesso!");
            votacao.atualizar();
        } else {
            alert(json.erro);
        }
    },
    atualizar: function () {
        location.reload(true);
    }
};
$(document).ready(votacao.inicializar);